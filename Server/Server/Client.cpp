#include "Client.h"

CClient::CClient()
{
	m_isReady = false;
	nickname = "NONE";
	m_ballCount = 0;
}

CClient::CClient(SOCKET _sock, int _id, SOCKADDR_IN _addr){
	m_isReady = false;
	nickname = "NONE";
	m_TCPSock = _sock;
	m_addr = _addr;
	m_id = _id;
}

CClient::~CClient()
{
}

SOCKET CClient::GetTCPSock() { return m_TCPSock; }
SOCKADDR_IN CClient::GetAddr() { return m_addr; }
int CClient::GetID() { return m_id; }
float CClient::GetxPos() { return m_x; }
float CClient::GetyPos() { return m_y; }
int CClient::GetBallCount() { return m_ballCount; }
bool CClient::GetIsReady() { return m_isReady; }
bool CClient::GetBallType() { return m_ballType; }

std::string CClient::GetNickname()
{
	return nickname;
}

int CClient::GetState() {

	return m_state;
}

void CClient::SetTCPSock(SOCKET s)
{
	m_TCPSock = s;
}

void CClient::SetAddr(SOCKADDR_IN sa)
{
	m_addr = sa;
}

void CClient::SetID(int id)
{
	m_id = id;
}

void CClient::SetIsReady(bool b)
{
	m_isReady = b;
}

void CClient::SetPos(float x, float y)
{
	m_x = x;
	m_y = y;
}

void CClient::SetBallCount(int b)
{
	m_ballCount = b;
}

void CClient::SetNickname(std::string nick)
{
	nickname = nick;
}

void CClient::SetState(int _state) {
	//m_statelock.lock();
	m_state = _state;
	//m_statelock.unlock();
}
