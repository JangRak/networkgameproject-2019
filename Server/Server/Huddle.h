#pragma once
#include <iostream>
#include "Ball.h"

class CHuddle
{
public:
	CHuddle();
	~CHuddle();

public:
	void SetInform(
		int index,
		int type,
		int moveDir,
		float x, float y,
		float moveMin, float moveMax);

	void GetInform(
		int* index,
		int* type,
		int* moveDir,
		float* x, float* y,
		float* moveMin, float* moveMax,
		long long* frame);

	void SetFrame(long long);

	int GetIndex() { return m_index; }
	int GetType() { return m_type; }
	int GetMoveDir() { return m_moveDir; }
	float GetxPos0() { return m_xPos0; }
	float GetyPos0() { return m_yPos0; }
	float GetzPos0() { return m_zPos0; }
	float GetxPos() { return m_xPos; }
	float GetyPos() { return m_yPos; }
	float GetzPos() { return m_zPos; }
	float GetMoveMin() { return m_moveMin; }
	float GetMoveMax() { return m_moveMax; }

	void Update(long long, float);
	bool IsCollision(float bx, float by, float bz);
	bool IsCollision(CBall* b);

private:
	int      m_index;
	int      m_type;
	int      m_moveDir;
	int      m_Dir;

	float   m_xPos0;
	float   m_yPos0;
	float   m_zPos0;

	float   m_xPos;
	float   m_yPos;
	float   m_zPos;

	float   m_moveMin;
	float   m_moveMax;
	float   m_rad = 0.f;

	float m_size;

	long long m_frame;
};
