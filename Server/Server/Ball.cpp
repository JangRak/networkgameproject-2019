#include "Ball.h"
#include "Protocol.h"
#include <iostream>
CBall::CBall() {}

CBall::~CBall() {}

// Public fuction

// Set
void CBall::SetPos(float _x, float _y, float _z)
{
	x = _x;
	y = _y;
	z = _z;
}

void CBall::SetPos0(float x, float y, float z)
{
	x0 = x;
	y0 = y;
	z0 = z;
}

void CBall::SetVel(float x, float y, float z)
{
	vel_x = x;
	vel_y = y;
	vel_z = z;
}

void CBall::SetType(bool type)
{
	m_itemtype = type;
}

void CBall::SetID(int id)
{
	m_playerID = id;
}

void CBall::SetFrame(long long _frame)
{
	frame = _frame;
}

void CBall::Update(long long _frame)
{
	//std::cout << x << ", " << y << std::endl;
	long long new_frame = _frame - frame;
	y = y0 + (new_frame * new_frame * GRAVITY / 2);
	if (y < -WINDOW_SIZE_Y + BALL_SIZE)
		y = -WINDOW_SIZE_Y + BALL_SIZE;

	z = z0 - (new_frame * BALL_INIT_Z_VEL);
}