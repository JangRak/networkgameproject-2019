#pragma once

#pragma comment(lib, "ws2_32")

#include <winsock2.h>
#include <string>
#include <mutex>

class CClient
{
public:
	CClient();
	CClient(SOCKET, int, SOCKADDR_IN);
	~CClient();

public:
	SOCKET GetTCPSock();
	SOCKADDR_IN GetAddr();
	int GetID();
	float GetxPos();
	float GetyPos();
	int GetBallCount();
	bool GetIsReady();
	bool GetBallType();
	std::string GetNickname();
	int GetState();

	void SetTCPSock(SOCKET s);
	void SetAddr(SOCKADDR_IN sa);
	void SetID(int id);
	void SetIsReady(bool b);
	void SetPos(float x, float y);
	void SetBallCount(int b);
	void SetNickname(std::string nick);
	void SetState(int);

private:
	SOCKET m_TCPSock;

	SOCKADDR_IN m_addr;

	int m_id;

	bool m_isReady;

	float m_x, m_y;
	int m_ballCount;  // 아이템 발동시 남은 아이템 볼 개수 
	bool m_ballType;

	std::string nickname;

	//std::mutex m_statelock;
	int m_state;
};