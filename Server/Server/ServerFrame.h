#pragma once

#pragma comment(lib, "ws2_32")
#include <WinSock2.h>
#include <mutex>
#include <unordered_map>
#include <chrono>
#include <fstream>
#include <set>
#include <string>

#include "Error.h"
#include "Client.h"
#include "Huddle.h"
#include "Protocol.h"

// 1s = 30 frame
#define CREATEFRAME_STAGEONE	30		
#define CREATEFRAME_STAGETWO	15		
#define CREATEFRAME_STAGETHREE	7		

enum STAGE { ONE, TWO, THREE };

class CServerFrame
{
public:
	CServerFrame();
	~CServerFrame();

public:
	int InitTCPServer();
	int InitUDPServer();

	//void SendPacket(int id, void* buff);

	static DWORD WINAPI UDPThread(LPVOID arg);
	static void UpdateMousePosition();

	// ClientThread
	static DWORD WINAPI Process(LPVOID arg);
	static void AddBall(int index, float xPos, float yPos, int id, long long);

	void RunLoginServer();
	void RunInGameServer();

	// InGame
	// Update
	//int IsFrame(std::chrono::time_point<std::chrono::steady_clock> s);
	void UpdatePlayerPos();
	void UpdateAddBall();
	void AddHuddles();
	void UpdateObjectsPos();
	void UpdateCollision();
	void UpdateScore(CHuddle* huddle);
	void UpdateBallCount();
	void InitData();

	void CreateUDPUpdateThread();

	void AddFrame();
	long long GetFrame();

private:
	static std::set<std::string> m_nick;

	SOCKET m_listenSock;		// Listen socket
	static SOCKET m_UDPsock;			// UDP socket
	static CError* m_error;		// Object that display error message

	static std::mutex m_ballLock;
	static AddBalls m_addBalls[MAX_ADD_BALL];
	static CBall* m_balls[MAX_BALL];

	static std::mutex m_ballCountLock;
	static int m_ballCount;

	CHuddle* m_huddles[MAX_HUDDLE];
	//std::mutex m_huddleLock;

	CHuddle m_stageOneHuddles[STAGE_HUDDLE_NUM];

	CHuddle* m_nextAddHuddleIndex;

	int m_stage;
	unsigned int m_createHuddleFrame;

	static std::unordered_map <int, CClient> m_mClients;	// Clients Information
	HANDLE m_hClientsThreads[MAX_USER];		// clients threads
	HANDLE m_hUDPThread;					// UDP Thread

	static std::mutex m_idLock;
	static int m_ID;

	static std::mutex m_stateLock;
	static bool m_InGame;

	std::mutex m_scoreLock;
	int m_score;

	std::mutex m_framelock;
	static long long m_frame;

	float huddlespeed;

	static bool m_gameStart;
	static PacketReadyStateServerToClient m_packetReady;
	PacketFrameState m_packetFrameState;
};