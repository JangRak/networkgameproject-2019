#define _WINSOCK_DEPRECATED_NO_WARNINGS // 최신 VC++ 컴파일 시 경고 방지

#define DELETE_RECT_Z 0

#include "ServerFrame.h"
#include <fstream>
#include <iterator>
#include <atomic>

CError*								CServerFrame::m_error;
std::mutex							CServerFrame::m_ballLock;
AddBalls							CServerFrame::m_addBalls[MAX_ADD_BALL];
CBall*								CServerFrame::m_balls[MAX_BALL];
int									CServerFrame::m_ballCount = 10;
std::mutex							CServerFrame::m_ballCountLock;
std::unordered_map <int, CClient>	CServerFrame::m_mClients;
std::mutex							CServerFrame::m_idLock;
int									CServerFrame::m_ID;
std::set<std::string>				CServerFrame::m_nick;

std::mutex							CServerFrame::m_stateLock;
bool								CServerFrame::m_InGame;

SOCKET								CServerFrame::m_UDPsock;

bool								CServerFrame::m_gameStart;
PacketReadyStateServerToClient		CServerFrame::m_packetReady;

long long							CServerFrame::m_frame;

extern HANDLE EveIngameLogic;

int itemPlayerID = 0;

enum HUDDLES {
	NONE,
	APPLE,
	TOMATO,
	GRAPE,
	WATERMELON,
	PUMPKIN,
	TANGERINE,
	ReCT,
	ITEM
};

CServerFrame::CServerFrame() : m_score(0)
{
	std::ifstream stage1("Huddle/Huddle_stage1.txt");
	std::ifstream stage2("Huddle/Huddle_stage2.txt");
	std::ifstream stage3("Huddle/Huddle_stage3.txt");

	CHuddle tmp;
	int index = 0;
	int type;
	float x;
	float y;
	int moveDir;
	float moveMin;
	float moveMax;

	// start stage 
	tmp.SetInform(
		0,
		-1,
		0,
		0, 0,
		0, 0
	);
	m_stageOneHuddles[index] = tmp;
	index++;

	if (stage1.is_open())
	{
		while (!stage1.eof())
		{
			stage1 >> type >> x >> y >> moveDir >> moveMin >> moveMax;

			tmp.SetInform(
				0,
				type,
				moveDir,
				x, y,
				moveMin, moveMax
			);

			m_stageOneHuddles[index] = tmp;
			index++;

		}
		stage1.close();
		// 더미 
		tmp.SetInform(
			0,
			0,
			0,
			0, 0,
			0, 0
		);
		for (int i = 0; i < 10; ++i)
		{
			m_stageOneHuddles[index] = tmp;
			index++;
		}

		// stage change
		tmp.SetInform(
			0,
			-2,
			0,
			0, 0,
			0, 0
		);
		m_stageOneHuddles[index] = tmp;
		index++;

	}
	else
		std::cout << "Stage1 No Read" << std::endl;

	// index = 0;
	if (stage2.is_open())
	{
		while (!stage2.eof())
		{
			stage2 >> type >> x >> y >> moveDir >> moveMin >> moveMax;

			tmp.SetInform(
				0,
				type,
				moveDir,
				x, y,
				moveMin, moveMax
			);

			m_stageOneHuddles[index] = tmp;
			index++;
		}
		stage2.close();
		// 더미 
		tmp.SetInform(
			0,
			0,
			0,
			0, 0,
			0, 0
		);
		 for (int i = 0; i < 10; ++i)
		 {
		 	m_stageOneHuddles[index] = tmp;
		 	index++;
		 }
		// stage change
		tmp.SetInform(
			0,
			-3,
			0,
			0, 0,
			0, 0
		);
		m_stageOneHuddles[index] = tmp;
		index++;
	}
	else
		std::cout << "Stage2 No Read" << std::endl;

	// index = 0;
	if (stage3.is_open())
	{
		while (!stage3.eof())
		{
			stage3 >> type >> x >> y >> moveDir >> moveMin >> moveMax;

			tmp.SetInform(
				0,
				type,
				moveDir,
				x, y,
				moveMin, moveMax
			);

			m_stageOneHuddles[index] = tmp;
			index++;
		}
		stage3.close();

		tmp.SetInform(
			0,
			-4,
			0,
			0, 0,
			0, 0
		);
		m_stageOneHuddles[index] = tmp;
		index++;
	}
	else
		std::cout << "Stage3 No Read" << std::endl;

	m_error = new CError;
	for (auto b : m_balls) {
		b = nullptr;
	}
	for (auto& d : m_addBalls) {
		memset(&d, -1, sizeof(AddBalls));
	}
	memset(&m_packetFrameState, -1, sizeof(PacketFrameState));

	m_stage = 0;
	m_createHuddleFrame = 0;
	m_InGame = false;
	m_frame = 0;

	huddlespeed = HUDDLE_MOVE_SPEED_1;

	m_gameStart = false;
	ZeroMemory(&m_packetReady, sizeof(PacketReadyStateServerToClient));
	m_nextAddHuddleIndex = m_stageOneHuddles;
}

CServerFrame::~CServerFrame()
{
	// Delete m_error
	if (nullptr != m_error) {
		delete m_error;
		m_error = nullptr;
	}

	// Delete m_balls
	for (auto b : m_balls) {
		if (nullptr != b) {
			delete b;
			b = nullptr;
		}
	}

	// Close thread handle
	for (auto& h : m_hClientsThreads) {
		CloseHandle(h);
		h = NULL;
	}

	// closesocket()
	closesocket(m_listenSock);

	// 윈속 종료
	WSACleanup();
}

// public function
int CServerFrame::InitTCPServer()
{
	std::wcout.imbue(std::locale("korean"));
	// Initialize WSA
	WSADATA wsa;
	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0) {
		return -1;
	}

	// socket()
	m_listenSock = socket(AF_INET, SOCK_STREAM, 0);
	if (m_listenSock == INVALID_SOCKET) {
		m_error->err_display("CServerFrame::InitTCPServer socket()");
	}

	// bind()
	SOCKADDR_IN serveraddr;
	ZeroMemory(&serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serveraddr.sin_port = htons(SERVERTCPPORT);
	int retval = bind(m_listenSock, (SOCKADDR *)&serveraddr, sizeof(serveraddr));
	if (retval == SOCKET_ERROR) {
		m_error->err_display("CServerFrame::InitTCPServer bind()");
	}

	// listen()
	retval = listen(m_listenSock, SOMAXCONN);
	if (retval == SOCKET_ERROR) {
		m_error->err_display("CServerFrame::InitTCPServer listen()");
	}

	// std::cout << "Create TCP Socket Success" << std::endl;

	return 1;
}

int CServerFrame::InitUDPServer()
{
	// socket()
	m_UDPsock = socket(AF_INET, SOCK_DGRAM, 0);
	if (m_UDPsock == INVALID_SOCKET) {
		m_error->err_display("CServerFrame::InitUDPServer socket()");
	}

	// bind()
	SOCKADDR_IN serveraddr;
	ZeroMemory(&serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serveraddr.sin_port = htons(SERVERUDPPORT);
	int retval = bind(m_UDPsock, (SOCKADDR *)&serveraddr, sizeof(serveraddr));
	if (retval == SOCKET_ERROR) {
		m_error->err_display("CServerFrame::InitUDPServer bind()");
	}

	// std::cout << "Create UDP Socket Success" << std::endl;

	return 1;
}

DWORD WINAPI CServerFrame::UDPThread(LPVOID arg)
{
	//printf("UDP 스레드 생성\n");

	// std::cout << "Do - Update UDP Thread" << std::endl;

	while (true) {
		UpdateMousePosition();
	}

	return 1;
}

// Ingame

void CServerFrame::UpdateMousePosition()
{
	SOCKADDR_IN clientAddr;
	int addrLength;
	int retval;

	PacketMoveMouse packet;
	ZeroMemory(&packet, sizeof(PacketMoveMouse));

	addrLength = sizeof(clientAddr);
	
	retval = recvfrom(m_UDPsock, (char*)&packet,
		sizeof(PacketMoveMouse), 0, (SOCKADDR*)&clientAddr, &addrLength);

	if (retval == SOCKET_ERROR)
		m_error->err_display("recvfrom() error in UDPThread ");

	int id = packet.id;
	float x = packet.x;
	float y = packet.y;

	//std::cout << x << ", " << y << std::endl;
	for (auto& c : m_mClients) {
		if (c.first == static_cast<int>(id))
			c.second.SetPos(x, y);
	}
}

void CServerFrame::RunLoginServer()
{
	// std::cout << "Do - Lobby State" << std::endl;

	SOCKET clientSock;
	SOCKADDR_IN clientAddr;
	int addrLength;

	u_long on = 1;
	int chagesocket = ioctlsocket(m_listenSock, FIONBIO, &on);
	if(chagesocket== SOCKET_ERROR)	
		m_error->err_display("CServerFrame::RunServer() changesocket");

	// 접속자 수 3명까지 accept 받고, 각 클라마다 스레드 생성하기.
	//while (MAX_USER != m_ID) {

	addrLength = sizeof(clientAddr);

	while (true) {
		DWORD RESULT = WaitForSingleObject(EveIngameLogic, 1);
		if (RESULT == WAIT_OBJECT_0) {
			return;
		}
		// accept()
		/*int clients_ready = 3;

		for (auto& c : m_mClients) {

			int client_state = c.second.GetState();

			if (client_state == 2)
				clients_ready--;

			else
				break;

			if (clients_ready == 0)
				return;
		}*/

		//while (true) {
			// std::cout << "accept" << std::endl;
			clientSock = accept(m_listenSock, (SOCKADDR *)&clientAddr, &addrLength);
			if (clientSock == INVALID_SOCKET) {
				if (WSAGetLastError() == WSAEWOULDBLOCK)
					continue;
				else {
					m_error->err_display("CServerFrame::RunServer() accept()");
					return ;
				}
			}
		//}

		on = 0;
		int chagesocket = ioctlsocket(clientSock, FIONBIO, &on);
		if (chagesocket == SOCKET_ERROR)
			m_error->err_display("CServerFrame::RunServer() changesocket1");

		int id = 3;
		
		for (int i = 0; i < 3; ++i) {
			if (m_mClients.count(i) == 0) {
				id = i;
				break;
			}
		}

		{
			// 접속중인 클라가 3명이면 접속 끊음
			if (2 < id) {
				closesocket(clientSock);
				continue;
			}
		}

		{
			// Print client Information
			printf("\n클라이언트 접속: IP 주소=%s, 포트 번호=%d, id=%d\n",
				inet_ntoa(clientAddr.sin_addr), ntohs(clientAddr.sin_port), id
			);

			// std::cout << "send" << std::endl;

			// ID 전송
			int retval = send(clientSock, (char*)&id, sizeof(int), 0);
			if (SOCKET_ERROR == retval) {
				/*if (WSAGetLastError() == WSAEWOULDBLOCK) {
					continue;
				}*/
				//else {
					m_error->err_display("CServerFrame::RunLoginServer send()");
					return;
				//}
			}
		
			// 클라이언트 정보를 저장하는 map에 저장
			CClient client(clientSock, id, clientAddr);
			m_mClients.emplace(id, client);


			//m_mClients[id].SetTCPSock(clientSock);
			//m_mClients[id].SetID(id);
			//m_mClients[id].SetAddr(clientAddr);

			// Client 1 : 1 TCPThread
			m_hClientsThreads[id] = CreateThread(NULL, 0, this->Process, (LPVOID)m_mClients[id].GetID(), 0, NULL);
			if (NULL == m_hClientsThreads[id]) {
				closesocket(clientSock);
			}
		}
	}
	// std::cout << "End - Lobby State Connect clients" << std::endl;
}

DWORD WINAPI CServerFrame::Process(LPVOID arg)
{
	int id = reinterpret_cast<int>(arg);
	printf("TCP 접속, ID : %d\n", id);

	SOCKADDR_IN clientAddr;
	int addrLength;

	// 클라이언트 정보 얻기
	addrLength = sizeof(clientAddr);
	getpeername(m_mClients[id].GetTCPSock(), (SOCKADDR *)&clientAddr, &addrLength);

	// std::cout << "[ client " << id << " ] Do - Login Logic" << std::endl;

	bool LoginCheck = false;
	while (!LoginCheck)	{
		PacketCanUseNick packet;
		ZeroMemory(&packet, sizeof(PacketCanUseNick));
		int retval = recv(m_mClients[id].GetTCPSock(), (char*)&packet, sizeof(PacketCanUseNick), 0);
		if (retval == 0 || retval == SOCKET_ERROR) {
			// std::cout << "Client " << id << " Login Out" << std::endl;
			m_error->err_display("CServerFrame::Process() recv() - Nickname recv");
			closesocket(m_mClients[id].GetTCPSock());
			m_mClients.erase(id);
			return 0;
		}
		// std::cout << "ID: " << id << " , Nickname: " << packet.Nick << std::endl;

		m_mClients[id].SetNickname(packet.Nick);

		int size = m_nick.size();
		m_nick.insert(packet.Nick);

		PacketLoginOK packet_ok;
		packet_ok.size = sizeof(PacketLoginOK);
		if (size == m_nick.size()) {
			//std::cout << "NO!" << std::endl;
			packet_ok.type = NICKNAME_UNUSE;
			retval = send(m_mClients[id].GetTCPSock(), (char*)&packet_ok, sizeof(PacketLoginOK), 0);
		}
		else {
			//std::cout << "YES! Lobby로!" << std::endl;
			packet_ok.type = NICKNAME_USE;
			retval = send(m_mClients[id].GetTCPSock(), (char*)&packet_ok, sizeof(PacketLoginOK), 0);
			LoginCheck = true;
		}
	}

	// std::cout << "[ client " << id << " ] Do - Lobby Logic" << std::endl;
	
	// 로비 클라이언트 ready정보 발신
	{
		for (auto& c : m_mClients) {
			int i = c.second.GetID();
			m_packetReady.id_r[i] = c.second.GetIsReady();
			strcpy(m_packetReady.Nick[i], m_mClients[i].GetNickname().c_str());
		}

		m_mClients[id].SetState(1);

		m_packetReady.type = CLIENT_READY_AND_NICK;
		for (auto& c : m_mClients) {
			if (1 == c.second.GetState()) {
				int retval = send(c.second.GetTCPSock(), (char*)&m_packetReady, sizeof(PacketReadyStateServerToClient), 0);
				if (SOCKET_ERROR == retval) {
					m_error->err_display("CServerFrame::Process() send() - Ready");
				}
			}
		}
	}

	while (true)
	{
		PacketReadyStateClientToServer packet;
		ZeroMemory(&packet, sizeof(PacketReadyStateClientToServer));
		int retval = recv(m_mClients[id].GetTCPSock(), (char*)&packet, sizeof(PacketReadyStateClientToServer), 0);
		if (retval == 0 || retval == SOCKET_ERROR) {
			// std::cout << "Client " << id << " Lobby Out" << std::endl;
			m_error->err_display("CServerFrame::Process() recv() - Ready recv");
			closesocket(m_mClients[id].GetTCPSock());
			m_nick.erase(m_mClients[id].GetNickname());
			m_mClients.erase(id);
			
			m_packetReady.type = CLIENT_READY_AND_NICK;
			m_packetReady.size = sizeof(PacketReadyStateServerToClient);

			for (auto& c : m_mClients) {
				int client_id = c.second.GetID();

				m_packetReady.id_r[client_id] = m_mClients[client_id].GetIsReady();
				strcpy(m_packetReady.Nick[client_id], m_mClients[client_id].GetNickname().c_str());
			}

			m_packetReady.id_r[id] = false;
			strcpy(m_packetReady.Nick[id], "None");


			for (auto& c : m_mClients) {
				if (1 == c.second.GetState()) {
					int retval = send(c.second.GetTCPSock(), (char*)&m_packetReady, sizeof(PacketReadyStateServerToClient), 0);
					if (SOCKET_ERROR == retval) {
						m_error->err_display("CServerFrame::Process() send() - lobby client out");
					}
				}
			}

			return 0;
		}
		
		switch (packet.type) {
		case CLIENT_READY:
			m_mClients[id].SetIsReady(true);
			break;

		case CLIENT_UNREADY:
			m_mClients[id].SetIsReady(false);
			break;
		}

		if (packet.type == CLIENTS_READY_OK)
			break;

		for (auto& c : m_mClients) {
			int i = c.second.GetID();
			m_packetReady.id_r[i] = c.second.GetIsReady();
		}

		m_packetReady.type = CLIENTS_AT_LEAST_ONE_UNREADY;
		for (auto& c : m_mClients) {
			int retval = send(c.second.GetTCPSock(), (char*)&m_packetReady, sizeof(PacketReadyStateServerToClient), 0);
			if (SOCKET_ERROR == retval) {
				m_error->err_display("CServerFrame::Process() send() - Ready");
			}
		}

		if (m_mClients.size() < 3) {
			continue;
		}

		m_stateLock.lock();
		if (m_InGame == true) {
			m_stateLock.unlock();
			continue;
		}

		for (int i = 0; i < MAX_USER; ++i) {
			if (false == m_mClients[i].GetIsReady()) {
				m_stateLock.unlock();
				break;
			}
			if (i == MAX_USER - 1) {
				m_InGame = true;
				m_stateLock.unlock();
			}
		}

		if (m_InGame) {
			m_packetReady.type = CLIENTS_ALL_READY;
			for (auto& c : m_mClients) {
				int retval = send(c.second.GetTCPSock(), (char*)&m_packetReady, sizeof(PacketReadyStateServerToClient), 0);
				if (SOCKET_ERROR == retval) {
					m_error->err_display("CServerFrame::Process() send() - Ready");
				}
			}
		}
	}

	SetEvent(EveIngameLogic);

	// std::cout << "[ client " << id << " ] Do - InGame Logic" << std::endl;

	// socket 번호를 통해서 고유 ID 알 수 있음. GetID()
	while (true) {
		// recv
		PacketClickMouse packet;
		ZeroMemory(&packet, sizeof(PacketClickMouse));
		int retval = recv(m_mClients[id].GetTCPSock(), (char*)&packet, sizeof(PacketClickMouse), 0);
		if (retval == 0) {
			std::cout << "Client " << id << " inGame Out" << std::endl;
			closesocket(m_mClients[id].GetTCPSock());
			m_mClients.erase(id);
			return 0;
		}
		else if (SOCKET_ERROR == retval) {
			m_error->err_display("CServerFrame::Process() recv() - Click");
			closesocket(m_mClients[id].GetTCPSock());
			m_mClients.erase(id);
			return 0;
		}

		if (packet.type == CLICK_MOUSE) {
			m_ballLock.lock();
			for (int i = 0; i < MAX_BALL; ++i)
			{
				if (NULL == m_balls[i])
				{
					m_balls[i] = new CBall;


					AddBall(i, packet.x, packet.y, id, m_frame);
					if (m_mClients[id].GetBallCount() > 0)
					{
						for (int j = i; j < MAX_BALL; ++j)
						{
							if (NULL == m_balls[j])
							{
								m_balls[j] = new CBall;
								AddBall(j, packet.x + 80, packet.y, id, m_frame);
								break;
							}

						}
						for (int k = i; k < MAX_BALL; ++k)
						{
							if (NULL == m_balls[k])
							{
								m_balls[k] = new CBall;
								AddBall(k, packet.x - 80, packet.y, id, m_frame);
								break;
							}
						}
						int ballcount = m_mClients[id].GetBallCount();
						m_mClients[id].SetBallCount(ballcount - 1);
					}
					m_ballLock.unlock();
					break;
				}

				if (i == MAX_BALL - 1)	
					m_ballLock.unlock();
			}
		}
	}

	return 1;
}

void CServerFrame::AddBall(int index, float xPos, float yPos, int id, long long frame)
{
	//std::cout << "AddBall : " << p->x << ", " << p->y << std::endl;
	//std::cout << index << std::endl;	

	m_ballCountLock.lock();
	m_ballCount -= 1;
	m_ballCountLock.unlock();

	m_balls[index]->SetID(id);
	m_balls[index]->SetPos(xPos, yPos, 0.f);
	m_balls[index]->SetPos0(xPos, yPos, 0.f);
	m_balls[index]->SetVel(0.f, BALL_INIT_Y_VEL, BALL_INIT_Z_VEL);
	m_balls[index]->SetFrame(frame);

	for (int i = 0; i < MAX_ADD_BALL; ++i) {
		if (0 > m_addBalls[i].playerid) {
			m_addBalls[i].index = index;
			m_addBalls[i].playerid = id;
			m_addBalls[i].itemType = m_mClients[id].GetBallType();
			m_addBalls[i].x = xPos;
			m_addBalls[i].y = yPos;
			break;
		}
	}
}

void CServerFrame::RunInGameServer()
{
	// std::cout << "Do - InGame State" << std::endl;

	auto start_time = std::chrono::high_resolution_clock::now();
	long long frame = 0;

	// Update Thread
	while (true) {
		auto end_time = std::chrono::high_resolution_clock::now();
		auto exec_time = end_time - start_time;
		int exec_ms = std::chrono::duration_cast<std::chrono::milliseconds>(exec_time).count();

		if (33 < exec_ms) {
			AddHuddles();
			UpdatePlayerPos();
			UpdateAddBall();
			UpdateObjectsPos();
			UpdateCollision();
			UpdateBallCount();

			for (auto& c : m_mClients) {
				int retval = send(c.second.GetTCPSock(), (char*)&m_packetFrameState, sizeof(PacketFrameState), 0);
				if (SOCKET_ERROR == retval) {
					m_error->err_display("CServerFrame::RunInGameServer send()");
				}
			}

			AddFrame();
			InitData();
			start_time = end_time;
		}
	}
}

// Update
void CServerFrame::UpdatePlayerPos()
{
	for (int i = 0; i < MAX_USER; ++i) {
		m_packetFrameState.xPos[i] = m_mClients[i].GetxPos();
		m_packetFrameState.yPos[i] = m_mClients[i].GetyPos();
	}
}

void CServerFrame::UpdateAddBall()
{
	m_ballLock.lock();
	for (int i = 0; i < MAX_ADD_BALL; ++i) {
		// m_addBalls[i].
		m_packetFrameState.addball[i] = m_addBalls[i];
	}
	m_ballLock.unlock();
}

void CServerFrame::AddHuddles()
{
	m_createHuddleFrame++;

	if (m_nextAddHuddleIndex->GetType() < 0) {

		switch (m_stage) {
		case 0:
			// std::cout << "STAGE 1" << std::endl;
			m_stage = 1;
			huddlespeed = HUDDLE_MOVE_SPEED_1;
			break;
		case 1:
			// std::cout << "STAGE 2" << std::endl;
			m_stage = 2;
			huddlespeed = HUDDLE_MOVE_SPEED_2;
			break;
		case 2:
			// std::cout << "STAGE 3" << std::endl;
			m_stage = 3;
			huddlespeed = HUDDLE_MOVE_SPEED_3;
			break;
		case 3:
			m_packetFrameState.type = INGAME_CLEAR;
			return;
			// 게임 끝
			break;
		}

		m_nextAddHuddleIndex++;
		return;
		//바뀐 state값 보내기
	}

	m_packetFrameState.stage = m_stage;
	m_packetFrameState.type = INGAME_RUNNING;

	if (m_createHuddleFrame < CREATEFRAME_STAGEONE)							return;
	if (&m_stageOneHuddles[STAGE_HUDDLE_NUM] < m_nextAddHuddleIndex)		return;

	m_createHuddleFrame = 0;

	int index = -1;

	//m_huddleLock.lock();
	for (int i = 0; i < MAX_HUDDLE; ++i) {
		if (m_huddles[i] == NULL) {
			index = i;
			break;
		}
	}
	//m_huddleLock.unlock();

	if (index < 0)	
		return;
	// std::cout << "+++create huddle ID : " << index << std::endl;

	std::cout << m_nextAddHuddleIndex->GetType() << std::endl;

	//m_huddleLock.lock();
	m_huddles[index] = new CHuddle();
	m_huddles[index]->SetInform(
		index,
		m_nextAddHuddleIndex->GetType(),
		m_nextAddHuddleIndex->GetMoveDir(),
		m_nextAddHuddleIndex->GetxPos0(), m_nextAddHuddleIndex->GetyPos0(),
		m_nextAddHuddleIndex->GetMoveMin(), m_nextAddHuddleIndex->GetMoveMax()
	);

	m_huddles[index]->SetFrame(m_frame);

	m_huddles[index]->GetInform(
		&m_packetFrameState.addobject.index,
		&m_packetFrameState.addobject.type,
		&m_packetFrameState.addobject.MoveDir,
		&m_packetFrameState.addobject.x, &m_packetFrameState.addobject.y,
		&m_packetFrameState.addobject.MoveMax, &m_packetFrameState.addobject.MoveMin,
		&m_packetFrameState.addobject.frame
	);
	//m_huddleLock.unlock();

	m_nextAddHuddleIndex++;
}

void CServerFrame::UpdateObjectsPos()
{
	//m_huddleLock.lock();
	for (int i = 0; i < MAX_HUDDLE; ++i) {
		if (nullptr != m_huddles[i]) {
			m_huddles[i]->Update(m_frame, huddlespeed);

			if (m_huddles[i]->GetzPos() >= DELETE_RECT_Z) {
				if (m_huddles[i]->GetType() > 0) {
					// 스코어
					m_ballCountLock.lock();
					switch (m_stage) {
					case 1:
						m_ballCount -= 5;
						break;
					case 2:
						m_ballCount -= 10;
						break;
					case 3:
						m_ballCount -= 20;
						break;
					default:
						std::cout << "stage error\n";
						while (true);
					}
					m_ballCountLock.unlock();
				}
				switch (m_huddles[i]->GetType()) {
				case -1: case -2: case -3:
					// std::cout << "Stage넘어가요! " << std::endl;
					break;
				default:
					// std::cout << "Number: " << m_huddles[i]->GetType() << " Huddle type error" << std::endl;
					// while (true);
					break;
				}

				delete m_huddles[i];
				m_huddles[i] = nullptr;
				m_packetFrameState.deletehuddle.index = i;
			}
		}
	}
	//m_huddleLock.unlock();

	//if(m_huddles[0])
	//	std::cout << m_huddles[0]->GetzPos() << std::endl;

	int deleteindex = 0;

	m_ballLock.lock();
	for (int i = 0; i < MAX_BALL; ++i) {
		if (nullptr != m_balls[i]) {
			m_balls[i]->Update(m_frame);

			if (DELETE_BALL_Z > m_balls[i]->RetPosz()) {
				// std::cout << "Delete ball" << std::endl;

				delete m_balls[i];
				m_balls[i] = nullptr;
				//m_delBalls[deleteindex++] = i;
				m_packetFrameState.deleteball[deleteindex++].index = i;
			}
		}
	}
	m_ballLock.unlock();
}

void CServerFrame::UpdateCollision()
{
	// 최대 15개 ( 0 ~ 14 )
	int collisionindex = 0;

	m_ballLock.lock();
	//m_huddleLock.lock();
	for (int i = 0; i < MAX_BALL; ++i) {
		if (!m_balls[i]) continue;
		for (int j = 0; j < MAX_HUDDLE; ++j) {
			if (!m_huddles[j]) continue;

			if (m_huddles[j]->IsCollision(m_balls[i])) {

				if (collisionindex < 15) {
					// std::cout << "collision" << std::endl;

					if (m_huddles[j]->GetType() == ITEM)
					 {
						m_mClients[m_balls[i]->GetID()].SetBallCount(5);
						// std::cout << "hahahahhahahahahahahhaha   : " << m_mClients[m_balls[i]->GetID()].GetBallCount() << std::endl;
					}
					else
					{
						UpdateScore(m_huddles[j]);
					}

					// std::cout << "INDEX :" << j << " Y : " << m_huddles[j]->GetyPos() << " Y0 : " << m_huddles[j]->m_yPos0 << std::endl;

					delete m_balls[i];
					m_balls[i] = nullptr;
					delete m_huddles[j];
					m_huddles[j] = nullptr;

					m_packetFrameState.collision[collisionindex].Ballindex = i;
					m_packetFrameState.collision[collisionindex].ObjIndex = j;

					collisionindex++;
				}
				break;
			}
		}
	}
	m_ballLock.unlock();
	//m_huddleLock.unlock();
}

void CServerFrame::UpdateScore(CHuddle* huddle)
{
	m_ballCountLock.lock();
	m_ballCount += 2 + ((m_stage - 1) * 2);
	m_ballCountLock.unlock();

	m_scoreLock.lock();
	switch (huddle->GetType()) {
	case APPLE:
		m_score += 25 + ((m_stage - 1) * 5);
		break;
	case TOMATO:
		m_score += 35 + ((m_stage - 1) * 5);
		break;
	case GRAPE:
		m_score += 30 + ((m_stage - 1) * 5);
		break;
	case WATERMELON:
		m_score += 15 + ((m_stage - 1) * 5);
		break;
	case PUMPKIN:
		m_score += 20 + ((m_stage - 1) * 5);
		break;
	case TANGERINE:
		m_score += 40 + ((m_stage - 1) * 5);
		break;
	case ReCT:
		m_score += 10 + ((m_stage - 1) * 5);
		break;
	case NONE:
		break;
	case -1: case -2: case -3:
		// std::cout << "Stage넘어가요! " << std::endl;
		break;
	default:
		// std::cout << "Number: " << huddle->GetType() << " Huddle type error" << std::endl;
		// while (true);
		break;
	}
	m_packetFrameState.score = m_score;
	m_scoreLock.unlock();
}

void CServerFrame::UpdateBallCount()
{
	m_ballCountLock.lock();
	m_packetFrameState.ballCount = m_ballCount;
	m_ballCountLock.unlock();

	m_scoreLock.lock();
	m_packetFrameState.score = m_score;
	m_scoreLock.unlock();
}

void CServerFrame::InitData()
{
	m_ballLock.lock();
	for (auto& d : m_addBalls) {
		memset(&d, -1, sizeof(AddBalls));
	}
	m_ballLock.unlock();

	memset(&m_packetFrameState, -1, sizeof(PacketFrameState));
}

void CServerFrame::CreateUDPUpdateThread() {

	m_hUDPThread = CreateThread(NULL, 0, this->UDPThread, 0, 0, NULL);
	if (NULL == m_hUDPThread) {
		// std::cout << "Create UDP Thread Failed" << std::endl;
	}

	// std::cout << "Create UDP Thread Success" << std::endl;
	/*else {
		CloseHandle(m_hUDPThread);
		m_hUDPThread = NULL;
	}*/
}

void CServerFrame::AddFrame()
{
	m_frame++;
}

long long CServerFrame::GetFrame()
{
	return m_frame;
}