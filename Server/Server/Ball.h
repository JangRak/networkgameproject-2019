#pragma once

//struct POSITION {
//	float x;
//	float y;
//	float z;
//};
//
//struct VELOCITY {
//	float vel_x;
//	float vel_y;
//	float vel_z;
//};

class CBall {
public:
	CBall();
	~CBall();

public:
	// Get
	void GetPos(float* _x, float* _y ,float* _z) {
		*_x = x;
		*_y = y;
		*_z = z;
	}

	void GetVel(float* _vx, float* _vy, float* _vz) {
		*_vx = vel_x;
		*_vy = vel_y;
		*_vz = vel_z;
	}

	float RetPosz() { return z; }

	char GetType() { return m_itemtype; }
	int GetID() { return m_playerID; }
	
	// Set
	void SetPos(float x, float y, float z);
	void SetPos0(float x, float y, float z);
	void SetVel(float x, float y, float z);
	void SetType(bool type);
	void SetID(int id);
	void SetFrame(long long);

	void Update(long long);

private:
	//POSITION m_p;
	//VELOCITY m_v;
	float x;
	float y;
	float z;
	float x0;
	float y0;
	float z0;
	float vel_x;
	float vel_y;
	float vel_z;

	float m_rad = 0.f;
	char m_itemtype;
	int m_playerID;
	long long frame;
};