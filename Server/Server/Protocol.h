
#define SERVERTCPPORT 15915
#define SERVERUDPPORT 51951
#define SERVERIP "127.0.0.1"


#define msPerFrame		33
#define MovePerFrame	0.033

// Lobby, InGame
#define CLIENTOUT				0


// Login
#define NICKNAME_CAN			1	// 나 이거 사용 가능해?
#define NICKNAME_USE			2	// 가능해!
#define NICKNAME_UNUSE			3	// 불가능해 ㅠㅠ	
#define NICKNAME_ADD			4   // 이건 뭘까 ㅎㅎ 

// Lobby
#define CLIENT_READY			5
#define CLIENT_UNREADY			6
#define CLIENTS_ALL_READY		7
#define CLIENTS_AT_LEAST_ONE_UNREADY		8
#define CLIENTS_READY_OK		111
#define CLIENT_READY_AND_NICK   60

// InGame
#define INGAME_RUNNING			300
#define INGAME_CLEAR			301
#define INGAME_GAMEOVER			302

#define MOVE_MOUSE				9
#define CLICK_MOUSE				10
#define FRAME_STATE				11
#define MULTI_ITEM				12

#define MAXCOLLISIONFRAME		10

#define STAGE_HUDDLE_NUM	70 * 3 + 40


// 장락 추가
#define MAX_BALL				100
#define MAX_HUDDLE				128
#define MAX_PARTICLE			100
#define MAX_BUFFER				1024
#define MAX_ADD_BALL			10
#define MAX_DEL_BALL			10
#define MAX_USER				3
#define DELETE_BALL_Z -8000
#define BALL_SIZE 30
#define GRAVITY MovePerFrame * -9.8 * 1

// 충돌체크 사이즈
#define PUMP_COLL 80
#define APP_COLL 60
#define TANGER_COLL 70
#define TOMA_COLL 75
#define GRAPE_COLL 60
#define ITEM_COLL 75
#define WATERM_COLL 105

#define WINDOW_SIZE_X 800
#define WINDOW_SIZE_Y 800
#define WINDOW_SIZE_Z 800

// Stage별 z축 장애물 이동 속도
#define HUDDLE_MOVE_SPEED_1 MovePerFrame * 1500
#define HUDDLE_MOVE_SPEED_2 MovePerFrame * 1800
#define HUDDLE_MOVE_SPEED_3 MovePerFrame * 2100

// 입력 장애물 이동 속도
#define HUDDLE_WAVE_MOVE 10

// 볼 속성
#define BALL_SIZE 30
#define BALL_INIT_Y_VEL MovePerFrame * 30
#define BALL_INIT_Z_VEL MovePerFrame * 3600
#define MUTIBALL_SIZE 90 // 아마 바꿔야할거같와

struct POSITION {
	float x;
	float y;
	float z;
};

struct VELOCITY {
	float vel_x;
	float vel_y;
	float vel_z;
};

// Packet

// Lobby + InGame

struct PacketClientOut{
	char size;
	char type;
	char id;
};

// Login

struct PacketCanUseNick {
	char size;
	char type;
	char Nick[16];
};

struct PacketLoginOK {
	char size;
	char type;
};

struct PacketALLNick {
	char size;
	char type;
	char Nick[3][16];
};

// Lobby
struct PacketReadyStateClientToServer {
	char size;
	char type;
	char id;
};

struct PacketReadyStateServerToClient {
	char size;
	char type;
	bool id_r[3];

	char Nick[3][16];
};

// InGame
//
struct PacketMoveMouse {
	char size;
	char type;
	char id;
	float x, y;
};
//
struct PacketClickMouse {
	char size;
	char type;
	char id;
	float x, y;
};

struct _CollisionObjects {
	int ObjIndex;
	char Ballindex;
};

typedef struct _AddBalls {
	char index;
	char playerid;
	bool itemType;
	float x, y;
}AddBalls;

struct _DeleteBalls {
	char index;
};

struct _DeleteHuddle {
	char index;
};

struct _AddObject {
	int index;
	int type;
	int MoveDir;
	float x, y;
	float MoveMin, MoveMax;
	long long frame;
};

//
struct PacketFrameState {
	char size;
	char type;	

	bool client_connected[3];

	float xPos[3];
	float yPos[3];

	int stage;// stage 값 넘길때 씁니다 ㅎㅎ
	int score;
	int ballCount;

	_CollisionObjects collision[15];
	_AddBalls addball[15];
	_DeleteBalls deleteball[15];
	_DeleteHuddle deletehuddle;
	_AddObject addobject;
};