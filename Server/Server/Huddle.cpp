#include "Huddle.h"
#include "Protocol.h"
#include <math.h>

#define RECT_SIZE 200
#define CREATE_HUDDLE_Z -8000

const float PI = 3.141592;

enum HUDDLES {
	NONE,
	APPLE,
	TOMATO,
	GRAPE,
	WATERMELON,
	PUMPKIN,
	TANGERINE,
	ReCT,
	ITEM
};

CHuddle::CHuddle()
{
}

CHuddle::~CHuddle()
{
}

//public funtion
void CHuddle::SetInform(
	int index,
	int type,
	int moveDir,
	float x, float y,
	float moveMin, float moveMax)
{
	m_index = static_cast<char>(index);
	m_type = type;
	m_moveDir = moveDir;
	m_xPos0 = x;
	m_yPos0 = y;
	m_yPos = y;
	m_zPos0 = CREATE_HUDDLE_Z;
	m_moveMin = moveMin;
	m_moveMax = moveMax;
	m_Dir = 0;

	switch (m_type) {
	case 0:
		m_zPos0 = -5;
		break;
	case APPLE:
		m_size = APP_COLL;
		break;
	case TOMATO:
		m_size = TOMA_COLL;
		break;
	case GRAPE:
		m_size = GRAPE_COLL;
		break;
	case WATERMELON:
		m_size = WATERM_COLL;
		break;
	case PUMPKIN:
		m_size = PUMP_COLL;
		break;
	case TANGERINE:
		m_size = TANGER_COLL;
		break;
	case ReCT:
		break;
	case ITEM:
		m_size = ITEM_COLL;
		break;
		//default:
		//   std::cout << "type error - SetInform" << std::endl;
		//   while (true);
	}
}

void CHuddle::GetInform(
	int* index,
	int* type,
	int* moveDir,
	float* x, float* y,
	float* moveMin, float* moveMax,
	long long* frame)
{
	*index = m_index;
	*type = m_type;
	*moveDir = m_moveDir;
	*x = m_xPos0;
	*y = m_yPos0;
	*moveMin = m_moveMin;
	*moveMax = m_moveMax;
	*frame = m_frame;
}

void CHuddle::SetFrame(long long _frame)
{
	m_frame = _frame;
}

void CHuddle::Update(long long frame, float huddlespeed)
{
	long long new_frame = frame - m_frame;
	m_zPos = m_zPos0 + (new_frame * huddlespeed);

	/*switch (m_Dir) {
	case 0:
	   break;
	case 1:
	   m_xPos = m_xPos0 + (m_moveMax * sin((m_zPos / 8000) * PI));
	   break;
	case 2:
	   m_yPos = m_yPos0 + (m_moveMax * sin((m_zPos / 8000) * PI));
	   break;
	default:
	   break;
	}*/

	/*switch (m_moveDir) {
	case 1:
	   if (m_Dir == 0) {
		  if (m_xPos < m_moveMax)   m_xPos += HUDDLE_WAVE_MOVE;
		  else m_Dir = 1;
	   }

	   if (m_Dir == 1) {
		  if (m_xPos > m_moveMin)   m_xPos -= HUDDLE_WAVE_MOVE;
		  else m_Dir = 0;
	   }
	   break;

	case 2:
	   if (m_Dir == 0) {
		  if (m_yPos < m_moveMax)   m_yPos += HUDDLE_WAVE_MOVE;
		  else m_Dir = 1;
	   }

	   if (m_Dir == 1) {
		  if (m_yPos > m_moveMin)   m_yPos -= HUDDLE_WAVE_MOVE;
		  else m_Dir = 0;
	   }
	   break;
	}*/

	// Recv���� ����
	//m_rad += 10.f;

}

bool CHuddle::IsCollision(float bx, float by, float bz)
{
	float collisionx = bx - this->m_xPos0;
	float collisiony = by - this->m_yPos0;
	float collisionz = bz - this->m_zPos;
	float collisionr = BALL_SIZE + m_size + 5.0f;
		
	if (collisionx * collisionx + collisiony * collisiony + collisionz * collisionz
		<= collisionr * collisionr)   return true;

	return false;
}

bool CHuddle::IsCollision(CBall* ball)
{
	float bx, by, bz;
	ball->GetPos(&bx, &by, &bz);

	switch (m_type) {
	case 0:
		return false;
	case ReCT:
		if ((by + BALL_SIZE / 2) < (m_yPos - RECT_SIZE))   return false;
		if ((by - BALL_SIZE / 2) > (m_yPos + RECT_SIZE))   return false;
		if ((bz + BALL_SIZE / 2) < (m_zPos - RECT_SIZE))   return false;
		if ((bz - BALL_SIZE / 2) > (m_zPos + RECT_SIZE))   return false;

		return true;

	case APPLE:
	case TOMATO:
	case GRAPE:
	case WATERMELON:
	case PUMPKIN:
	case TANGERINE:
	case ITEM:
		return IsCollision(bx, by, bz);
		//default:
		//   std::cout << "type error - IsCollision" << std::endl;
		//   while (true);
	}
}