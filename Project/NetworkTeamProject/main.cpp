#include "main.h"
#include <sstream>
#include <mutex>

VIEW View = Perspective;

COLOR playercolor1{ 1.f,0.4f,0.4f,1.f };
COLOR playercolor2{ 0.4f,1.0f,0.4f,1.f };
COLOR playercolor3{ 0.1f,0.1f,1.f,1.f };

HUDDLE* huddle[MAX_HUDDLE];
BALL* ball[MAX_BALL];
PARTICLE* particle[MAX_PARTICLE];
Player* player[3] =
{ new Player(-300.f, -400.f, 20.f, playercolor1.r, playercolor1.g, playercolor1.b, playercolor1.a),
	new Player(0.f, -400.f, 20.f, playercolor2.r, playercolor2.g, playercolor2.b, playercolor2.a),
	new Player(300.f, -400.f, 20.f, playercolor3.r, playercolor3.g, playercolor3.b, playercolor3.a) };

int move_x = 0, move_y = 0, move_z = 240;
Angle Angle_x, Angle_y, Angle_z;

int CurrentState = 1;

int Have_balls = 10;
int Score = 0;
int Huddle_speed = HUDDLE_MOVE_SPEED_1;

bool multiball = false;
int count_multiball = 0;

float title_state_z = -8000;
float over_state_z = -8000;
float back_ground_z = 0;

bool end_state_background_switch = false;
bool end_state_score_switch = false;

bool SWITCH_Light0 = true;
GLfloat AmbientLight0[] = { 0.4f, 0.4f, 0.4f, 1.f }; // 빛의 세기 + 빛의 색
GLfloat DiffuseLight0[] = { 1.f, 1.f, 1.f, 1.0f }; // 광원 색
GLfloat SpecularLight0[] = { 0.0, 0.0, 0.0, 1.0 }; // 하이라이트 색
GLfloat lightPos0[] = { LIGHT_X, LIGHT_Y, LIGHT_Z + 600, 1.0 }; // 위치: (10, 5, 20) 

GLfloat in_fog_color[4] = { 0.74f, 0.74f, 1.f, 1.0 };
GLfloat fog_density = 0.2f;
GLfloat fog_start = 500, f;
GLfloat fog_end = -CREATE_HUDDLE_Z + 1000.f;

bool overlapNickname = false;
string prevNick = "";

// STATE state = LOGIN_STATE;
//STATE state = 1;

SERVER server;
int my_id;

bool IscharacterMove = true;
// 인트로 문구가 멈추면 캐릭터 움직이기 위해서 나중에 변경해도 좋음
// ingame으로 시작하려면 true로 해주고 해주세용
bool IamReady = false;

string nickname;
bool keyCursor = true;
#define CURSORTIMER 500
int cursorTimer = CURSORTIMER;

void main(int argc, char **argv) {

	my_id = server.ConnectServer();

	server.CreateRecvThread();

	srand((unsigned)time(NULL));

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH); // 디스플레이 모드 설정
	glutInitWindowPosition(300, 0);
	glutInitWindowSize(WINDOW_SIZE_X, WINDOW_SIZE_Y);
	glutCreateWindow("FRUITS CRUSH");

	// 마우스 커서 안보이게하기
	//glutSetCursor(GLUT_CURSOR_NONE);

	glutDisplayFunc(drawScene);
	glutKeyboardFunc(Keyboard);
	glutMouseFunc(Mouse);
	glutPassiveMotionFunc(CharacterMotion);
	glutTimerFunc(16, Timer, 0);
	glutReshapeFunc(Reshape);
	//PlaySound(TEXT("gameover.wav"), NULL, SND_ASYNC);

	glutMainLoop();
}

GLvoid drawScene(GLvoid) {
	glClearColor(0.6f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);

	GLfloat ambientLight[] = { 0.1f, 0.1f, 0.1f, 1.f };
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientLight);
	glLightModelf(GL_LIGHT_MODEL_LOCAL_VIEWER, 1.0);
	glLightModelf(GL_LIGHT_MODEL_TWO_SIDE, 0.0);

	// 직각투영
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glOrtho(-WINDOW_SIZE_X, WINDOW_SIZE_X, -WINDOW_SIZE_Y, WINDOW_SIZE_Y, -WINDOW_SIZE_Z, WINDOW_SIZE_Z);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//FOG
	{
		glEnable(GL_FOG);

		glFogf(GL_FOG_MODE, GL_LINEAR);

		glFogfv(GL_FOG_COLOR, in_fog_color);
		glFogf(GL_FOG_START, fog_start);
		glFogf(GL_FOG_END, fog_end);

		glFogf(GL_FOG_DENSITY, fog_density);
	}

	//그리기
	{
		// 2D 그리기
		{
			if (server.GetState() == 3)
			{
				if (overlapNickname)
				{
					string overlap = " Duplicate nicknames!!! : ";
					drawText(overlap.data(), overlap.size(), 0 - 250, 600);

					drawText(prevNick.data(), prevNick.size(), 200, 600);
				}

				string out;
				out = "Please write your nickname : ";
				drawText(out.data(), out.size(), 0 - 250, 500);
				drawText(nickname.data(), nickname.size(), 0 - 250, 400);
				if (keyCursor)
				{
					// keyCursor = false;
					string cursor = "|";
					drawText(cursor.data(), cursor.size(), 0 - 250 + nickname.size() * 19, 400);
				}
				// drawText(nickname.data(), nickname.size(), )
			}


			if (server.GetState() == 0) {

				for (int i = 0; i < MAX_USER; ++i) {

					string strReadystate;

					bool PlayerReady;
					player[i]->GetReady(&PlayerReady);

					if (PlayerReady)	strReadystate = "YES!";
					else				strReadystate = "NO~";

					for (int i = 0; i < MAX_USER; ++i)
					{
						string finalNickname = "NickName: ";
						finalNickname += player[i]->GetNickname();
						drawText(finalNickname.data(), finalNickname.size(), -500 + (400 * i), -500);
					}


					string strReady;
					strReady = "Ready? " + strReadystate;
					drawText(strReady.data(), strReady.size(), -500 + (400 * i), -600);

				}
			}

			else if (server.GetState() == 1) {
				stringstream sstr;
				string ballnum;
				sstr << Have_balls;
				ballnum = sstr.str();

				string ball;
				ball = "Ball: " + ballnum;
				drawText(ball.data(), ball.size(), -400, 700);

				string stage = "Current Stage: ";
				stage += std::to_string(CurrentState);
				drawText(stage.data(), stage.size(), -200, 700);


				stringstream sstr2;
				string allscore;
				sstr2 << Score;
				allscore = sstr2.str();

				string strScore;
				strScore = "Score: " + allscore;
				drawText(strScore.data(), strScore.size(), 300, 700);

			}

			else if (server.GetState() == 2) {
				if (end_state_score_switch) {
					stringstream str;
					string score;
					str << Score;
					score = str.str();

					string total_score;
					total_score = "SCORE: " + score;
					drawText(total_score.data(), total_score.size(), -70, -250);
				}
			}
		}
	}
	glPushMatrix();

	// 원근투영
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0, 1.0, 1.0, 10000.0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(move_x, move_y, move_z + 900.f, 0.f, 0.f, 300.f, 0.f, 1.f, 0.f);

	glTranslatef(-move_x, -move_y, -move_z);
	glRotatef(Angle_x.radian, 1.f, 0.f, 0.f);
	glRotatef(Angle_y.radian, 0.f, 1.f, 0.f);
	glRotatef(Angle_z.radian, 0.f, 0.f, 1.f);

	// LIGHT0
	{
		if (CurrentState == 1)
		{
			AmbientLight0[0] = 0.4f;
			AmbientLight0[1] = 0.4f; 
			AmbientLight0[2] = 0.4f; 
			AmbientLight0[3] = 1.f;
				// = { 0.4f, 0.4f, 0.4f, 1.f };
		}
		else if (CurrentState == 2)
		{
			AmbientLight0[0] = 0.4f;
			AmbientLight0[1] = 1.f;
			AmbientLight0[2] = 0.4f;
			AmbientLight0[3] = 1.f;
		}
		else if (CurrentState == 3)
		{
			AmbientLight0[0] = 1.f;
			AmbientLight0[1] = 0.4f;
			AmbientLight0[2] = 0.4f;
			AmbientLight0[3] = 1.f;
		}

		glLightfv(GL_LIGHT0, GL_AMBIENT, AmbientLight0);
		glLightfv(GL_LIGHT0, GL_DIFFUSE, DiffuseLight0);
		glLightfv(GL_LIGHT0, GL_SPECULAR, SpecularLight0);
		glLightfv(GL_LIGHT0, GL_POSITION, lightPos0);
		if (SWITCH_Light0)      glEnable(GL_LIGHT0);
		else glDisable(GL_LIGHT0);

	}
	//그리기
	{
		if (server.GetState() == 0) {
			Write_FRUIT_CRUSH(0.f, 550.f, title_state_z);

			for (int i = 0; i < MAX_CLIENTS; ++i)
				if (player[i])	player[i]->Draw();
		}

		else if (server.GetState() == 1) {

			// Write_FRUIT_CRUSH(0, 0, title_state_z);
			for (int i = 0; i < MAX_CLIENTS; ++i)
				if (player[i])	player[i]->Draw();
		}

		else if (server.GetState() == 2) {

			Write_FRUIT_CRUSH(0, WINDOW_SIZE_Y / 2, title_state_z);
			Write_GAME_CLEAR(0, -WINDOW_SIZE_Y + 150, over_state_z);
			if (end_state_background_switch)   Draw_End_state_Background(back_ground_z);
		}
	}

	DrawObjects();
	Draw_bottom();

	glPopMatrix();
	glutSwapBuffers();
}


GLvoid Reshape(int w, int h)
{
	glViewport(0, 0, w, h);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0, 1.0, 1.0, 10000.0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.f, 0.f, 600.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f);
}

void Mouse(int button, int m_state, int x, int y) {

	int mx = 2 * (x - WINDOW_SIZE_X / 2);
	int my = -2 * (y - WINDOW_SIZE_Y / 2);
	if (button == GLUT_LEFT_BUTTON && m_state == GLUT_DOWN) {

		if (server.GetState() == 0 && IscharacterMove) {
			//float x, y = 0.f;
			//player[my_id]->GetPosition(&x, &y);

			//server.SendTCPClickMouse(my_id, mx, my);
		}

		if (server.GetState() == PLAY_STATE) {

			// if (server.GetState() == 1) {
			float x, y = 0.f;
			player[my_id]->GetPosition(&x, &y);

			server.SendTCPClickMouse(my_id, mx, my);
		}

		else if (server.GetState() == 2) {

		}
	}

	glutPostRedisplay();
}

void CharacterMotion(int x, int y) {

	int mx = 2 * (x - WINDOW_SIZE_X / 2);
	int my = -2 * (y - WINDOW_SIZE_Y / 2);

	server.SendUDPMoveMouse(my_id, mx, my);

	glutPostRedisplay();
}

void Timer(int value) {
	Change_Angle_xyz();

	if (server.GetState() == 3)
	{
		if (cursorTimer < 0)
		{
			keyCursor = 1 - keyCursor;
			cursorTimer = CURSORTIMER;
		}
		cursorTimer -= CURSORTIMER / 40;
	}

	if (server.GetState() == 0) {
		// 게임 제목 이동
		if (title_state_z < 200.f)
			title_state_z += 100.f;
		else {
			IscharacterMove = true;
		}

	}

	else if (server.GetState() == 1) {
		// 게임 제목 이동
		// if (title_state_z <= 1500.f) title_state_z += 100.f;

		// 아이템 획득하면, 멀티볼 타이머 동작
		//if (multiball) {
		//	count_multiball = (count_multiball + 1) % TIME_MULTIBALL;	// 실제 시간으로 바꿔야해 
		//	if (count_multiball == 0)   multiball = false;
		//}

		UpdateObjects();
	}

	else if (server.GetState() == 2) {
		// 게임오버 이동
		if (over_state_z <= 500.f) over_state_z += 100.f;
		// 게임오버 따라서 카메라 이동
		if (-500 <= over_state_z && over_state_z <= 500.f) {
			end_state_background_switch = true;
			back_ground_z += 100.f;
			move_z += 50.f;
		}
		if (over_state_z > 500.f)   end_state_score_switch = true;
	}

	if (Have_balls < 0) Have_balls = 0;

	glutPostRedisplay();
	glutTimerFunc(16, Timer, 0);
}

void Keyboard(unsigned char key, int x, int y)
{
	if (server.GetState() == 3)
	{
		LoginKeboardEvent(key);


	}
	else
	{
		switch (key)
		{

		case '1':
			SWITCH_Light0 = (SWITCH_Light0 + 1) % 2;
			break;

		case 'x':   case 'X':
			Angle_x.sw = (Angle_x.sw + 1) % 2;
			break;
		case 'y':   case 'Y':
			Angle_y.sw = (Angle_y.sw + 1) % 2;
			break;
		case 'z':   case 'Z':
			Angle_z.sw = (Angle_z.sw + 1) % 2;
			break;

		case 'a': case 'A':
			move_x -= 10;
			break;

		case 'd': case 'D':
			move_x += 10;
			break;

		case 'w': case 'W':
			move_y += 10;
			break;

		case 's': case 'S':
			move_y -= 10;
			break;

		case '+': case '=':
			move_z += 10;
			break;

		case '-': case '_':
			move_z -= 10;
			break;

		case 'i': case 'I':
			move_x = 0.f;
			move_y = 0.f;
			move_z = 240.f;
			Angle_x.radian = 0.f;
			Angle_y.radian = 0.f;
			Angle_z.radian = 0.f;
			break;
		case 27:	// esc
			exit(1);
			break;
		case 'r': case 'R':
			//if (server.GetState() == 0) {
			if (IamReady == false) {
				IamReady = true;
				server.SendTCPClientReady(my_id);
			}
			else if (IamReady == true) {
				IamReady = false;
				server.SendTCPClientUnReady(my_id);
			}
			//}
			break;
		}
	}
	glutPostRedisplay();
}

// 카메라 회전
void Change_Angle_xyz()
{
	if (Angle_x.sw) Angle_x.radian += 2;
	if (Angle_y.sw) Angle_y.radian += 2;
	if (Angle_z.sw) Angle_z.radian += 2;

	glutPostRedisplay();
}

// 사각 기둥 그리기
void Draw_bottom() {
	GLfloat Object1[] = { 0.5f, 0.5f, 1.f, 1.f };
	GLfloat Object_specref1[] = { 0.5f, 0.3f, 0.3f, 1.f };
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object1);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref1);
	glMateriali(GL_FRONT, GL_SHININESS, 64);

	glPushMatrix();
	glTranslatef(0.f, 0.f, -8000);
	glScalef(1500.f, 1500.f, 1.f);
	glutSolidCube(1);
	glPopMatrix();

	for (float rad = 0.f; rad < 360.f; rad += 90.f) {
		glPushMatrix();
		glRotatef(rad, 0.f, 0.f, 1.f);
		glTranslatef(0.f, -WINDOW_SIZE_Y, -MAP_Z_SIZE / 2);
		glScalef(2 * WINDOW_SIZE_X, 1.f, MAP_Z_SIZE);
		glColor3f(1.f, rad / 100.f, 0.f);
		glutSolidCube(1.f);
		glPopMatrix();
	}
}

// 오브젝트 그리기
void DrawObjects() {
	for (int i = 0; i < MAX_HUDDLE; ++i)
		if (huddle[i])	huddle[i]->Draw();
	for (int i = 0; i < MAX_BALL; ++i)
		if (ball[i])		ball[i]->Draw();
	for (int i = 0; i < MAX_PARTICLE; ++i)
		if (particle[i])		particle[i]->Draw();
}


// 오브젝트 업데이트 (과일 & 아이템 & 파티클)
void UpdateObjects() {
	long long frame = server.GetFrame();
	//std::cout << frame << std::endl;
	for (int i = 0; i < MAX_BALL; i++) {
		if (ball[i]) {
			ball[i]->Update(frame);
			/*if (ball[i]->z <= DELETE_BALL_Z) {
				delete ball[i];
				ball[i] = NULL;
			}*/
		}
	}

	for (int i = 0; i < MAX_PARTICLE; i++) {
		if (particle[i]) {
			particle[i]->Update();
			//	선택지 1
			/*if (particle[i]->z >= DELETE_PARTICLE_Z) {
				delete particle[i];
				particle[i] = nullptr;
			}*/

			//	선택지 2
			if (particle[i]->IsDelete()) {
				delete particle[i];
				particle[i] = nullptr;
			}
		}
	}

	for (int i = 0; i < MAX_HUDDLE; i++) {
		if (huddle[i]) {
			huddle[i]->Update(frame, Huddle_speed);

			if (huddle[i]->z >= DELETE_RECT_Z) {
				if (huddle[i]->type != Item)
					Have_balls -= 30;
				delete huddle[i];
				huddle[i] = NULL;
			}
		}
	}

	//if(huddle[0])
	//std::cout << huddle[0]->z << std::endl;
}

void drawText(const char *text, int length, int x, int y)
{
	glRasterPos2i(x, y);
	for (int i = 0; i < length; i++)
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, (int)text[i]);

	// GLUT_BITMAP_
}

// void drawText(const char *text, int length, int x, int y, )
// {
// 	glRasterPos2i(x, y);
// 	for (int i = 0; i < length; i++)
// 		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, (int)text[i]);
// 
// 	
// }

// 시작할때 글씨쓰기
void Write_FRUIT_CRUSH(float x, float y, float z)
{
	// Fruit
	GLfloat Object_specref[] = { 1.f, 1.f, 1.f, 1.f };

	GLfloat Object0[] = { 0.7f, 0.2f, 0.2f, 1.f };
	GLfloat Object1[] = { 1.f, 1.f, 0.2f, 1.f };
	GLfloat Object2[] = { 0.2f, 1.f, 1.f, 1.f };
	GLfloat Object3[] = { 1.f, 0.2f, 1.f, 1.f };
	GLfloat Object4[] = { 02.f, 1.f, 0.2f, 1.f };
	GLfloat Object5[] = { 0.2f, 0.2f, 0.7f, 1.f };
	GLfloat Object6[] = { 0.5f, 0.5f, 0.f, 1.f };
	GLfloat Object7[] = { 0.2f, 0.5f, 0.5f, 1.f };
	GLfloat Object8[] = { 0.5f, 02.f, 0.5f, 1.f };
	GLfloat Object9[] = { 0.2f, 0.5f, 0.2f, 1.f };

	glPushMatrix();
	glTranslatef(x - 300, y, z);

	// F
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object0);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref);
	glMateriali(GL_FRONT, GL_SHININESS, 64);

	glPushMatrix();
	glTranslatef(-5, 0, 0);
	glScalef(4, 1, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-35, -55, 0);
	glScalef(1, 6, 1);
	glutSolidCube(20);
	glPopMatrix();


	glPushMatrix();
	glTranslatef(-5, -45, 0);
	glScalef(3, 1, 1);
	glutSolidCube(20);
	glPopMatrix();

	// R
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object1);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref);
	glMateriali(GL_FRONT, GL_SHININESS, 64);

	glPushMatrix();
	glTranslatef(60, -75, 0);
	glScalef(1, 4.2, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(80, -55, 0);
	glScalef(2.5, 1, 1);
	glutSolidCube(20);
	glPopMatrix();

	// U
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object2);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref);
	glMateriali(GL_FRONT, GL_SHININESS, 64);

	glPushMatrix();
	glTranslatef(140, -75, 0);
	glScalef(1, 3, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(160, -110, 0);
	glScalef(2, 1, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(180, -75, 0);
	glScalef(1, 3, 1);
	glutSolidCube(20);
	glPopMatrix();

	// i
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object3);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref);
	glMateriali(GL_FRONT, GL_SHININESS, 64);

	glPushMatrix();
	glTranslatef(230, -45, 0);
	glScalef(1, 1, 1);
	glutSolidCube(20);
	glPopMatrix();


	glPushMatrix();
	glTranslatef(230, -90, 0);
	glScalef(1, 3, 1);
	glutSolidCube(20);
	glPopMatrix();

	// t
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object4);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref);
	glMateriali(GL_FRONT, GL_SHININESS, 64);

	glPushMatrix();
	glTranslatef(290, -65, 0);
	glScalef(3, 1, 1);
	glutSolidCube(20);
	glPopMatrix();


	glPushMatrix();
	glTranslatef(290, -80, 0);
	glScalef(1, 4.5, 1);
	glutSolidCube(20);
	glPopMatrix();

	// 이게마지막임
	glPopMatrix();

	// Crush
	glPushMatrix();
	glTranslatef(x, y - 200, z);

	//C
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object5);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref);
	glMateriali(GL_FRONT, GL_SHININESS, 64);

	glPushMatrix();
	glTranslatef(0, 0, 0);
	glScalef(5, 1.3, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-50, -50, 0);
	glScalef(1.3, 5, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0, -100, 0);
	glScalef(5, 1.3, 1);
	glutSolidCube(20);
	glPopMatrix();

	// R
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object6);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref);
	glMateriali(GL_FRONT, GL_SHININESS, 64);

	glPushMatrix();
	glTranslatef(90, -75, 0);
	glScalef(1.3, 4.2, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(110, -55, 0);
	glScalef(2.5, 1.3, 1);
	glutSolidCube(20);
	glPopMatrix();

	// U
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object7);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref);
	glMateriali(GL_FRONT, GL_SHININESS, 64);

	glPushMatrix();
	glTranslatef(170, -75, 0);
	glScalef(1.3, 3.2, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(190, -110, 0);
	glScalef(2, 1.3, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(210, -75, 0);
	glScalef(1.3, 3.2, 1);
	glutSolidCube(20);
	glPopMatrix();

	// S
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object8);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref);
	glMateriali(GL_FRONT, GL_SHININESS, 64);

	glPushMatrix();
	glTranslatef(280, -55, 0);
	glScalef(2.5, 1, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(255, -65, 0);
	glScalef(1, 1, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(280, -85, 0);
	glScalef(2.5, 1, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(305, -105, 0);
	glScalef(1, 1, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(280, -115, 0);
	glScalef(2.5, 1, 1);
	glutSolidCube(20);
	glPopMatrix();

	// H
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object9);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref);
	glMateriali(GL_FRONT, GL_SHININESS, 64);

	glPushMatrix();
	glTranslatef(350, -80, 0);
	glScalef(1, 5, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(370, -90, 0);
	glScalef(2.5, 1, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(390, -110, 0);
	glScalef(1, 2, 1);
	glutSolidCube(20);
	glPopMatrix();

	// 지우지마
	glPopMatrix();
}

// 끝날때 글씨쓰기 gameover
void Write_GAME_OVER(float x, float y, float z)
{
	glPushMatrix();

	GLfloat Object_specref1[] = { 1.f, 1.f, 1.f, 1.f };
	GLfloat Object1[] = { 0.7f, 0.2f, 0.2f, 1.f };

	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object1);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref1);
	glMateriali(GL_FRONT, GL_SHININESS, 64);

	glTranslatef(x - 200, y + 200, z);

	// G
	glPushMatrix();
	glScalef(4, 1.3, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-40, -50, 0);
	glScalef(1.3, 5, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0, -100, 0);
	glScalef(4, 1.3, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(37, -80, 0);
	glScalef(1.3, 3, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(30, -60, 0);
	glScalef(2, 1.3, 1);
	glutSolidCube(20);
	glPopMatrix();

	// A
	glPushMatrix();
	glTranslatef(120, 0, 0);
	glScalef(4, 1.3, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(120, -40, 0);
	glScalef(3.4, 1.3, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(85, -60, 0);
	glScalef(1.3, 5, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(155, -60, 0);
	glScalef(1.3, 5, 1);
	glutSolidCube(20);
	glPopMatrix();

	// M
	glPushMatrix();
	glTranslatef(200, -60, 0);
	glScalef(1.3, 5, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(240, -60, 0);
	glScalef(1.3, 5, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(280, -60, 0);
	glScalef(1.3, 5, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(240, 0, 0);
	glScalef(5, 1.3, 1);
	glutSolidCube(20);
	glPopMatrix();

	// E
	glPushMatrix();
	glTranslatef(370, 0, 0);
	glScalef(4.5, 1.3, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(330, -55, 0);
	glScalef(1.3, 4.5, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(370, -100, 0);
	glScalef(4.5, 1.3, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(370, -50, 0);
	glScalef(4.5, 1.3, 1);
	glutSolidCube(20);
	glPopMatrix();


	// 지우지마라
	glPopMatrix();

	// over
	glPushMatrix();

	GLfloat Object_specref[] = { 1.f, 1.f, 1.f, 1.f };
	GLfloat Object0[] = { 0.2f, 0.2f, 0.7f, 1.f };

	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object0);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref);
	glMateriali(GL_FRONT, GL_SHININESS, 64);

	glTranslatef(x - 100, y, z);

	// O
	glPushMatrix();
	glTranslatef(-10, 0, 0);
	glScalef(4.5, 1.3, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-50, -50, 0);
	glScalef(1.3, 5, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-10, -100, 0);
	glScalef(4.5, 1.3, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(30, -50, 0);
	glScalef(1.3, 5, 1);
	glutSolidCube(20);
	glPopMatrix();

	// V
	glPushMatrix();
	glTranslatef(85, -40, 0);
	glScalef(1.3, 5.5, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(155, -40, 0);
	glScalef(1.3, 5.5, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(120, -100, 0);
	glScalef(4, 1.3, 1);
	glutSolidCube(20);
	glPopMatrix();

	// E
	glPushMatrix();
	glTranslatef(250, 0, 0);
	glScalef(4.5, 1.3, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(210, -55, 0);
	glScalef(1.3, 4.5, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(250, -100, 0);
	glScalef(4.5, 1.3, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(250, -50, 0);
	glScalef(4.5, 1.3, 1);
	glutSolidCube(20);
	glPopMatrix();

	// R
	glPushMatrix();
	glTranslatef(360, 0, 0);
	glScalef(3.5, 1.3, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(330, -55, 0);
	glScalef(1.3, 6, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(360, -50, 0);
	glScalef(3.5, 1.3, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(390, -30, 0);
	glScalef(1.3, 2, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(390, -85, 0);
	glRotatef(20, 0, 0, 1);
	glScalef(1.3, 3, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPopMatrix();
}

// 끝날때 글씨 쓰기 game END
void Write_GAME_CLEAR(float x, float y, float z)
{
	glPushMatrix();

	GLfloat Object_specref1[] = { 1.f, 1.f, 1.f, 1.f };
	GLfloat Object1[] = { 0.7f, 0.2f, 0.2f, 1.f };

	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object1);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref1);
	glMateriali(GL_FRONT, GL_SHININESS, 64);

	glTranslatef(x - 200, y + 200, z);
	{
		// G
		glPushMatrix();
		glScalef(4, 1.3, 1);
		glutSolidCube(20);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-40, -50, 0);
		glScalef(1.3, 5, 1);
		glutSolidCube(20);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0, -100, 0);
		glScalef(4, 1.3, 1);
		glutSolidCube(20);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(37, -80, 0);
		glScalef(1.3, 3, 1);
		glutSolidCube(20);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(30, -60, 0);
		glScalef(2, 1.3, 1);
		glutSolidCube(20);
		glPopMatrix();

		// A
		glPushMatrix();
		glTranslatef(120, 0, 0);
		glScalef(4, 1.3, 1);
		glutSolidCube(20);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(120, -40, 0);
		glScalef(3.4, 1.3, 1);
		glutSolidCube(20);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(85, -60, 0);
		glScalef(1.3, 5, 1);
		glutSolidCube(20);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(155, -60, 0);
		glScalef(1.3, 5, 1);
		glutSolidCube(20);
		glPopMatrix();

		// M
		glPushMatrix();
		glTranslatef(200, -60, 0);
		glScalef(1.3, 5, 1);
		glutSolidCube(20);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(240, -60, 0);
		glScalef(1.3, 5, 1);
		glutSolidCube(20);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(280, -60, 0);
		glScalef(1.3, 5, 1);
		glutSolidCube(20);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(240, 0, 0);
		glScalef(5, 1.3, 1);
		glutSolidCube(20);
		glPopMatrix();

		// E
		glPushMatrix();
		glTranslatef(370, 0, 0);
		glScalef(4.5, 1.3, 1);
		glutSolidCube(20);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(330, -55, 0);
		glScalef(1.3, 4.5, 1);
		glutSolidCube(20);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(370, -100, 0);
		glScalef(4.5, 1.3, 1);
		glutSolidCube(20);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(370, -50, 0);
		glScalef(4.5, 1.3, 1);
		glutSolidCube(20);
		glPopMatrix();

	}
	// 지우지마라
	glPopMatrix();

	// over
	glPushMatrix();

	GLfloat Object_specref[] = { 1.f, 1.f, 1.f, 1.f };
	GLfloat Object0[] = { 0.2f, 0.2f, 0.7f, 1.f };

	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object0);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref);
	glMateriali(GL_FRONT, GL_SHININESS, 64);

	glTranslatef(x - 100, y, z);



	// E
	glPushMatrix();
	glTranslatef(30, 0, 0);
	glScalef(4.5, 1.3, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-10, -55, 0);
	glScalef(1.3, 4.5, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(30, -100, 0);
	glScalef(4.5, 1.3, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(30, -50, 0);
	glScalef(4.5, 1.3, 1);
	glutSolidCube(20);
	glPopMatrix();

	// N
	glPushMatrix();
	glTranslatef(100, -55, 0);
	glScalef(1.3, 6.5, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(140, -45, 0);
	glRotatef(45.f, 0.f, 0.f, 1.f);
	glScalef(1.3, 5.5, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(190, -55, 0);
	glScalef(1.3, 6.5, 1);
	glutSolidCube(20);
	glPopMatrix();


	// D
	glPushMatrix();
	glTranslatef(240, -55, 0);
	glScalef(1.3, 6.5, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(280, 0, 0);
	glScalef(2.5, 1.3, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(280, -100, 0);
	glScalef(2.5, 1.3, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(320, -10, 0);
	glRotatef(45.f, 0.f, 0.f, 1.f);
	glScalef(1.3, 2.0, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(340, -55, 0);
	glScalef(1.3, 2.5, 1);
	glutSolidCube(20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(320, -90, 0);
	glRotatef(-45.f, 0.f, 0.f, 1.f);
	glScalef(1.3, 2.0, 1);
	glutSolidCube(20);
	glPopMatrix();



	glPopMatrix();
}

// 맵 뒤에 벽 그리기
void Draw_End_state_Background(float move_z) {
	GLfloat Object[] = { 0.5f, 0.5f, 1.f, 1.f };
	GLfloat Object_specref[] = { 0.5f, 0.3f, 0.3f, 1.f };
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref);
	glMateriali(GL_FRONT, GL_SHININESS, 64);

	glPushMatrix();
	glTranslatef(0.f, 0.f, -8000.f + move_z);
	glScalef(15000.f, 15000.f, 1.f);
	glutSolidCube(1);
	glPopMatrix();
}

void LoginKeboardEvent(unsigned char key) {
	server.CreateNick(key);
}


