#pragma once
#include "HUDDLE.h"

class NONE : public HUDDLE {
public:
	NONE();
	NONE(float in_x, float in_y, float in_z, int in_moving, float in_data_min, float in_data_max, long long frame);
	~NONE();

	void Draw();
};
