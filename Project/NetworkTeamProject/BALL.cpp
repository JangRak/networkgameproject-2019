#include "BALL.h"
#include "main.h"


extern COLOR playercolor1;
extern COLOR playercolor2;
extern COLOR playercolor3;

BALL::BALL(){
}

BALL::BALL(float in_x, float in_y, bool in_type, int in_playerID, long long in_frame)
	:x(in_x), y(in_y), y0(in_y), z0(0.f), itemtype(in_type), player(in_playerID), frame(in_frame){
	z = 0.f;
	vel_y = BALL_INIT_Y_VEL;
	vel_z = BALL_INIT_Z_VEL;

	if (in_playerID == 0) {
		r = playercolor1.r;
		g = playercolor1.g;
		b = playercolor1.b;
		a = playercolor1.a;
	}
	else if (in_playerID == 1) {
		r = playercolor2.r;
		g = playercolor2.g;
		b = playercolor2.b;
		a = playercolor2.a;
	}
	else if (in_playerID == 2) {
		r = playercolor3.r;
		g = playercolor3.g;
		b = playercolor3.b;
		a = playercolor3.a;
	}
}


BALL::~BALL()
{
}

void BALL::Draw(){
	GLfloat Object[] = { r, g, b, a };
	/*if (itemtype) {
		Object[0] = 0.7f;
		Object[2] = 0.1f;
	}*/

	GLfloat Object_specref[] = { 1.f, 1.f, 1.f, 1.f };
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref);
	glMateriali(GL_FRONT, GL_SHININESS, 64);

	glPushMatrix();
	glTranslatef(x, y, z);
	glRotatef(rad, 1.f, 0.f, 0.f);
	glutSolidSphere(BALL_SIZE, 30, 30);
	glPopMatrix();
}

void BALL::Update(long long _frame){

	long long new_frame = _frame - frame;
	y = y0 + (new_frame * new_frame * GRAVITY / 2);
	if (y < -WINDOW_SIZE_Y + BALL_SIZE)
		y = -WINDOW_SIZE_Y + BALL_SIZE;

	z = z0 - (new_frame * BALL_INIT_Z_VEL);

	/*
	vel_y += GRAVITY;
	y = y + vel_y;

	if (y < -WINDOW_SIZE_Z) {
		y = -WINDOW_SIZE_Z + BALL_SIZE;
		vel_y = -0.75 * vel_y;
	}

	z = z - vel_z;
	*/
}
