#pragma once
#include "HUDDLE.h"

class APPLE : public HUDDLE{
public:
	APPLE();
	APPLE(float in_x, float in_y, float in_z, int in_moving, float in_data_min, float in_data_max, long long frame);
	~APPLE();

	void Draw();
};
