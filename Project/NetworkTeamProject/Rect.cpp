#include "Rect.h"
#include "main.h"
void OBSRECT::Draw() {
	
	GLfloat Object_specref[] = { 1.f, 1.f, 1.f, 1.f };
	GLfloat Object_specref0[] = { 0.f, 0.f, 0.f, 1.f };

	glPushMatrix();
	glTranslatef(x, y, z);
	
	
	GLfloat Object3[] = { 0.3f, 0.3f, 0.7f, 1.f };
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object3);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref0);
	glMateriali(GL_FRONT, GL_SHININESS, 64);

	glScalef(2*WINDOW_SIZE_X, RECT_SIZE, 1);
	glutSolidCube(1);

	glPopMatrix();
}

bool OBSRECT::Collision(const BALL& ball) const{
	if (ball.y + BALL_SIZE < y - RECT_SIZE)	return false;
	if (ball.y - BALL_SIZE > y + RECT_SIZE)	return false;
	if (ball.z + BALL_SIZE < z - RECT_SIZE)	return false;
	if (ball.z - BALL_SIZE > z + RECT_SIZE)	return false;

	return true;
}

OBSRECT::OBSRECT() {

}

OBSRECT::OBSRECT(float in_x, float in_y, float in_z, int in_moving, float in_data_min, float in_data_max, long long in_frame)
	:HUDDLE(in_x, in_y, in_z, in_moving, in_data_min, in_data_max, in_frame) {
	
	type = Rect;
	data = 0;
	size = 60;
	score = 50;
}


OBSRECT::~OBSRECT()
{
}

