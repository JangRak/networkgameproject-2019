#pragma once
#include "HUDDLE.h"

class OBSRECT : public HUDDLE {
public:
	OBSRECT();
	OBSRECT(float in_x, float in_y, float in_z, int in_moving, float in_data_min, float in_data_max, long long frame);
	~OBSRECT();
	
	void Draw();
	bool Collision(const BALL&)const;
};
