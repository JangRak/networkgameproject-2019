#pragma once

#include <mutex>

enum STATE { LOBBY_STATE, PLAY_STATE, END_STATE, LOGIN_STATE };

struct TCP;
struct UDP;
struct PacketALLNick;
struct PacketLoginOK;
// struct STATE;


class SERVER {
private:
	TCP* tcp;
	UDP* udp;
	char serverip[15];
	int state;
	std::mutex statelock;
	
	long long frame;
	std::mutex framelock;

public:
	SERVER();
	virtual ~SERVER();

	void SetState(int);
	int GetState();

	void AddFrame();
	long long GetFrame();

	void GetServerIP();
	int ConnectServer();
	
	void CreateRecvThread();

	void SendTCPClickMouse(const char& id, const float& x, const float& y);
	void SendUDPMoveMouse(const char& id, const float& x, const float& y);
	void SendTCPClientReady(int id);
	void SendTCPClientUnReady(int id);
	void CreateNick(unsigned char);
	void ClearNick();

	void SendCanUseNicknameClientToServer(const char* nickname);
	PacketLoginOK RecvNickOK();
	PacketALLNick RecvClientsNicknameClientToServer();
};

