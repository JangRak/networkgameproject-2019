#include "TOMATO.h"
#include "main.h"
void TOMATO::Draw() {
	GLfloat Object_specref[] = { 1.f, 1.f, 1.f, 1.f };
	GLfloat Object_specref0[] = { 0.f, 0.f, 0.f, 1.f };

	glPushMatrix();
	float lengh = 40.f;
	glTranslatef(x, y, z);
	glutWireSphere(size, 10, 10);
	glRotatef(90.f, 1.f, 1.f, 0.f);
	glRotatef(rad, 0.f, 1.f, 0.f);
	glScalef(0.5f, 0.5f, 0.5f);

	GLfloat Object3[] = { 1.f, 0.01f, 0.01f, 1.f };
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object3);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref);
	glMateriali(GL_FRONT, GL_SHININESS, 64);

	for (float deg = 0.f; deg < 360.f; deg += 90.f) {
		glPushMatrix();
		glScalef(0.9, 1.3, 1.f);
		float x = static_cast<float> (lengh * cos(deg(deg)));
		float z = static_cast<float> (lengh * sin(deg(deg)));
		glTranslatef(x, 0, z);
		glutSolidSphere(110, 100, 100);
		glPopMatrix();
	}
	glutWireSphere(size, 10, 10);

	GLfloat Object1[] = { 0.01f, 1.f, 0.01f, 1.f };
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object1);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref0);
	glMateriali(GL_FRONT, GL_SHININESS, 64);
	for (float i = 0.f; i < 180.f; i += 30.f) {
		glPushMatrix();
		glTranslatef(0, 100, 0);
		glRotatef(i, 0.f, 1.f, 0.f);
		glScalef(0.2f, 1.f, 1.f);
		glutSolidSphere(size + 5, 100, 100);
		glPopMatrix();
	}

	glPopMatrix();



}

TOMATO::TOMATO() 
	:HUDDLE() {
}

TOMATO::TOMATO(float in_x, float in_y, float in_z, int in_moving, float in_data_min, float in_data_max, long long in_frame)
	: HUDDLE(in_x, in_y, in_z, in_moving, in_data_min, in_data_max, in_frame) {
	type = Tomato; 
	data = 0;
	size = TOMA_COLL;
	score = 70;
}


TOMATO::~TOMATO()
{
}
