#pragma once

class BALL{
public:
	float x, y, z;
	float y0, z0;
	float vel_x, vel_y, vel_z;
	long long frame;
	float rad = 0.f;
	bool itemtype;
	int player;

	float r, g, b, a;

	BALL();
	BALL(float in_x, float in_y, bool in_type, int _playerID, long long in_frame);
	~BALL();

	void Draw();
	void Update(long long);
};

