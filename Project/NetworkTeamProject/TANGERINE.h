#pragma once
#include "HUDDLE.h"

class TANGERINE : public HUDDLE {
public:
	TANGERINE();
	TANGERINE(float in_x, float in_y, float in_z, int in_moving, float in_data_min, float in_data_max, long long frame);
	~TANGERINE();

	void Draw();
};
