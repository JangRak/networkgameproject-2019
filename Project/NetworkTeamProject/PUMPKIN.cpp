#include "PUMPKIN.h"
#include "main.h"
PUMPKIN::PUMPKIN()
	:HUDDLE() {
}
PUMPKIN::PUMPKIN(float in_x, float in_y, float in_z, int in_moving, float in_data_min, float in_data_max, long long in_frame)
	: HUDDLE(in_x, in_y, in_z, in_moving, in_data_min, in_data_max, in_frame) {
	type = Pumkin; 
	data = 0;
	size = PUMP_COLL;
	score = 50;
}


PUMPKIN::~PUMPKIN()
{
}

void PUMPKIN::Draw() {
	float lengh = 40.f;
	GLfloat Object_specref[] = { 1.f, 1.f, 1.f, 1.f };

	glPushMatrix();
	glTranslatef(x, y, z);
	glRotatef(rad, 0.f, 1.f, 0.f);
	glutWireSphere(size, 10, 10);

	GLfloat Object3[] = { 1.f, 0.5f, 0.01f, 1.f };
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object3);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref);
	glMateriali(GL_FRONT, GL_SHININESS, 64);
	for (float deg = 0.f; deg < 360.f; deg += 60.f) {
		glPushMatrix();
		float mx = static_cast<float> (lengh * cos(deg(deg)));
		float mz = static_cast<float> (lengh * sin(deg(deg)));
		glTranslatef(mx, 0, mz);
		glutSolidSphere(50, 100, 100);
		glPopMatrix();
	}
	GLfloat Object0[] = { 0.01f, 1.f, 0.01f, 1.f };
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object0);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref);
	glMateriali(GL_FRONT, GL_SHININESS, 64);

	glPushMatrix();
	glTranslatef(0, 100, 0);

	glPushMatrix();
	glRotatef(90.f, 1.f, 0.f, 0.f);
	glutSolidCone(10, 80, 10, 10);
	glPopMatrix();

	glPopMatrix();
	glPopMatrix();

}