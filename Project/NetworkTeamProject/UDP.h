#pragma once

struct WSAData;
struct sockaddr_in;

class UDP{
	WSAData* wsa;
	unsigned int sock;
	sockaddr_in* serveraddr;

public:
	UDP();
	~UDP();

	void err_quit(const char*);

	void ConnectUDPToServer(const char*);

	void MoveMouse(char, float, float);
};

