#include "HUDDLE.h"
#include "main.h"

const float PI = 3.141592;

HUDDLE::HUDDLE(){

}

HUDDLE::HUDDLE(float in_x, float in_y, float in_z, int in_moving, float in_data_min, float in_data_max, long long in_frame)
	:x(in_x), y(in_y), z(in_z), moving(in_moving), data_min(in_data_min), data_max(in_data_max), frame(in_frame) {
	x0 = in_x;
	y0 = in_y;
	z0 = in_z;
}

void HUDDLE::Draw(){
	
}

bool HUDDLE::Collision(const BALL& ball) const {
	float collisionx = ball.x - this->x;
	float collisiony = ball.y - this->y;
	float collisionz = ball.z - this->z;
	float collisionr = BALL_SIZE + this->size;

	if (collisionx * collisionx + collisiony * collisiony + collisionz * collisionz
		<= collisionr * collisionr)	return true;

	return false;
}

void HUDDLE::Update(long long _frame, float huddlespeed){
	/*switch (moving) {
	case 1:
		if (data == 0) {
			if (x < data_max)   x += HUDDLE_WAVE_MOVE;
			else data = 1;
		}

		if (data == 1) {
			if (x > data_min)   x -= HUDDLE_WAVE_MOVE;
			else data = 0;
		}
		break;

	case 2:
		if (data == 0) {
			if (y < data_max)   y += HUDDLE_WAVE_MOVE;
			else data = 1;
		}

		if (data == 1) {
			if (y > data_min)   y -= HUDDLE_WAVE_MOVE;
			else data = 0;
		}
		break;
	}*/
	//rad += 10.f;
	//z += Huddle_speed;

	long long new_frame = _frame - frame;
	z = z0 + (new_frame * huddlespeed);

	/*switch (moving) {
	case 0:
		break;
	case 1:
		x = x0 + (data_max * sin((z / 8000) * PI));
		break;
	case 2:
		y = y0 + (data_max * sin((z / 8000) * PI));
		break;
	default:
		break;
	}*/
}

void HUDDLE::GetPos(float* _x, float* _y, float* _z) {
	*_x = x;
	*_y = y;
	*_z = z;
}
