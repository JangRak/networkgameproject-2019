#include "Player.h"
#include "main.h"

Player::Player()
{
}

Player::Player(float x, float y, float size, float r, float g, float b, float a)
	: m_x(x), m_y(y), m_size(size), m_r(r), m_g(g), m_b(b), m_a(a)
{
	m_z = 0.f;
	haveball = 0;
	ready = false;
	strcpy(nickname, "NONE");
}


Player::~Player()
{
}

void Player::GetPosition(float* x, float* y)
{
	*x = m_x;
	*y = m_y;
}

void Player::SetPosition(float x, float y)
{
	m_x = x;
	m_y = y;
}

void Player::GetReady(bool* _ready ){
	*_ready = ready;
}

void Player::SetReady(bool _ready){
	ready = _ready;
}

void Player::SetSize(float size)
{
	m_size = size;
}

void Player::SetColor(float r, float g, float b)
{
	m_r = r;
	m_g = g;
	m_b = b;
}

void Player::SetHaveballs(int num){
	haveball = num;
}


char* Player::GetNickname()
{
	return nickname;
}

void Player::SetNickname(char nick[16])
{
	strcpy(nickname, nick);
}

void Player::Draw()
{
	GLfloat Object_specref[] = { 1.f, 1.f, 1.f, 1.f };
	GLfloat Object_specref0[] = { 0.f, 0.f, 0.f, 1.f };
	//glPushMatrix();
	glPushMatrix();
	GLfloat Object3[] = { m_r, m_g, m_b, m_a };
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object3);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref);
	glMateriali(GL_FRONT, GL_SHININESS, 64);
	glTranslated(m_x, m_y, m_z);
	glutSolidCube(m_size);
	glPopMatrix();

	//glPopMatrix();
}

void Player::Update()
{

}
