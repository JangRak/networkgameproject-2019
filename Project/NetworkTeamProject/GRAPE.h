#pragma once
#include "HUDDLE.h"

class GRAPE : public HUDDLE {
public:
	GRAPE();
	GRAPE(float in_x, float in_y, float in_z, int in_moving, float in_data_min, float in_data_max, long long frame);
	~GRAPE();

	void Draw();
};
