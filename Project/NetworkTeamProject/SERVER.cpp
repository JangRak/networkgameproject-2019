#include "SERVER.h"

#pragma comment(lib,"ws2_32")
#include <WinSock2.h>
#include <iostream>
#include "..\..\Server\Server\Protocol.h"

#include "TCP.h"
#include "UDP.h"

extern std::string nickname;
extern bool overlapNickname;
extern std::string prevNick;

SERVER::SERVER() : serverip("127.0.0.1"){
	tcp = new TCP();
	udp = new UDP();
	state = 3;
	frame = 0;
}


SERVER::~SERVER() {
	tcp->~TCP();
	udp->~UDP();

	delete tcp;
	delete udp;
}

void SERVER::SetState(int _state){
	state = _state;
}

int SERVER::GetState(){
	return state;
}

void SERVER::AddFrame() {
	//framelock.lock();
	frame++;
	//framelock.unlock();
}

long long SERVER::GetFrame() {
	//std::lock_guard<std::mutex> flock(framelock);
	return frame;
}

void SERVER::GetServerIP(){
	std::cout << "server ip : ";
	std::cin >> serverip;
}

int SERVER::ConnectServer(){
	//GetServerIP();
	int clientid = tcp->ConnectTCPToServer(serverip);
	udp->ConnectUDPToServer(serverip);

	return clientid;
}

void SERVER::CreateRecvThread(){
	tcp->CreateRecvThread();
}

void SERVER::SendTCPClickMouse(const char& id, const float& x, const float& y){
	tcp->ClickMouse(id, x, y);
}

void SERVER::SendUDPMoveMouse(const char& id, const float& x, const float& y){
	udp->MoveMouse(id, x, y);
}

void SERVER::SendTCPClientReady(int id){
	tcp->SendReadyClientToServer(id);
}

void SERVER::SendTCPClientUnReady(int id){
	tcp->SendUnReadyClientToServer(id);
}

void SERVER::CreateNick(unsigned char key){

	if (key == '/' || key == ' ') {
		if (nickname.size() > 0) {
			SendCanUseNicknameClientToServer(nickname.c_str());
		}
	}
	else {
		char tmp = key;
		nickname.push_back(tmp);
	}
}

void SERVER::ClearNick() {
	overlapNickname = true;
	prevNick = nickname;
	nickname.clear();
}

void SERVER::SendCanUseNicknameClientToServer(const char * nickname)
{
	tcp->SendTCPCanUseNicknameClientToServer(nickname);
}

PacketLoginOK SERVER::RecvNickOK()
{
	overlapNickname = false;

	return tcp->RecvNickOK();
}

PacketALLNick SERVER::RecvClientsNicknameClientToServer()
{
	return tcp->RecvTCPClientsNicknameClientToServer();
}

