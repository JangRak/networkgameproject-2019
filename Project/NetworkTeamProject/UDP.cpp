#include "UDP.h"

#pragma comment(lib,"ws2_32")
#include <WinSock2.h>
#include <iostream>
#include "..\..\Server\Server\Protocol.h"


UDP::UDP(){
	wsa = new WSAData;
	serveraddr = new sockaddr_in;
}

UDP::~UDP(){
	delete wsa;
	delete serveraddr;
}

void UDP::err_quit(const char *msg) {
	LPVOID lpMsgBuf;
	// 오류메세지 얻기
	FormatMessage(
		// 오류메세지 저장 공간을 알아서 할당 | 운영체제로부터 오류 메세지 가져옴
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		// NULL , 오류코드 - [구체적 오류 코드 확인]
		NULL, WSAGetLastError(),
		//오류메세지 표시 언어 - 사용자 제어판 기본 설정 언어
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		// 오류 문자열 시작주소, nSize, Arguments
		(LPTSTR)&lpMsgBuf, 0, NULL);
	// 메세지 상자 생성 - [오류 메세지 확인]
	MessageBox(NULL, (LPCTSTR)lpMsgBuf, (LPCTSTR)msg, MB_ICONERROR);
	// 시스템 할당 메모리 반환
	LocalFree(lpMsgBuf);
	exit(1);
}

void UDP::ConnectUDPToServer(const char* serverip) {

	if (WSAStartup(MAKEWORD(2, 2), &(*wsa)) != 0)
		return;

	sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock == INVALID_SOCKET)	err_quit("[UDP Socket Error]");
	
	ZeroMemory(&(*serveraddr), sizeof((*serveraddr)));
	serveraddr->sin_family = AF_INET;
	serveraddr->sin_addr.s_addr = inet_addr(serverip);
	serveraddr->sin_port = htons(SERVERUDPPORT);
	int retval = connect(sock, (SOCKADDR *)&(*serveraddr), sizeof((*serveraddr)));
	if (retval == SOCKET_ERROR)		err_quit("[UDP Connect Error]");

	std::cout << "UDP connect Success" << std::endl;
}

void UDP::MoveMouse(char id, float x, float y){
	PacketMoveMouse packet;
	packet.size = sizeof(PacketMoveMouse);
	packet.type = MOVE_MOUSE;
	packet.id = id;
	packet.x = x;
	packet.y = y;
	int retval = send(sock, (char*)&packet, sizeof(PacketMoveMouse), 0);
	if (retval == SOCKET_ERROR)		err_quit("[UDP Send Error]");
}
