#pragma once

class Player{
private:
	float m_x, m_y, m_z;
	int haveball;
	float m_size;
	float m_r, m_g, m_b, m_a;
	bool ready;
	char nickname[16];

public:
	Player();
	Player(float x, float y, float size,  float r, float g, float b, float a);
	~Player();

public:
	void GetPosition(float* x, float* y);
	void SetPosition(float x, float z);

	void GetReady(bool*);
	void SetReady(bool);

	void SetSize(float size);
	void SetColor(float r, float g, float b);
	void SetHaveballs(int num);

	char* GetNickname();
	void SetNickname(char nick[16]);

	void Draw();
	void Update();
};

