#include "NONE.h"
#include "main.h"

NONE::NONE()
{
}

NONE::NONE(float in_x, float in_y, float in_z, int in_moving, float in_data_min, float in_data_max, long long frame)
	:HUDDLE(in_x, in_y, in_z, in_moving, in_data_min, in_data_max, frame) {
	type = None;
	data = 0;
	size = 0.f;
	score = 0.f;
}

NONE::~NONE()
{
}

void NONE::Draw()
{

}