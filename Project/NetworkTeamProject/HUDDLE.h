#pragma once
class BALL;

class HUDDLE {
public:
	int type;
	float x, y, z;		// 장애물의 위치
	float x0, y0, z0;
	float rad = 0.f;	// 회전 각도
	float size;
	float rotation = 0.f;
	float data_min, data_max;	// 좌우 / 상하로 움직일 때 최대/최소로 갈 수 있는 거리의 양
	int score;
	int moving;		// 1번은 x축을 중심으로, 2번은 y축을 중심으로 움직임
	int data;		// 움직이는 방향인듯
	long long frame;

public:
	HUDDLE();
	HUDDLE(float in_x, float in_y, float in_z, int in_moving, float in_data_min, float in_data_max, long long frame);
	
	virtual void Draw();
	virtual bool Collision(const BALL&)const;
	void Update(long long, float);
	void GetPos(float*, float*, float*);
};
