#pragma once
#include "HUDDLE.h"

class ITEM : public HUDDLE{
public:
	ITEM();
	ITEM(float in_x, float in_y, float in_z, int in_moving, float in_data_min, float in_data_max, long long frame);
	~ITEM();

	void Draw();
};

