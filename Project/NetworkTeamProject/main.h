#pragma once
//Include
#include <gl/freeglut.h>
#include <time.h>

#include <iostream>
#include <random>
#include <fstream>
#include <stdio.h>
#include <Windows.h>
#include <mmsystem.h>
#pragma comment(lib,"winmm.lib")

#include <string>

#include "SERVER.h"
#include "Player.h"

#include "HUDDLE.h"
#include "APPLE.h"
#include "GRAPE.h"
#include "PUMPKIN.h"
#include "Rect.h"
#include "TANGERINE.h"
#include "TOMATO.h"
#include "WATERMELLON.h"
#include "BALL.h"
#include "ITEM.h"

#include "Particle.h"

#include "..\..\Server\Server\Protocol.h"

using namespace std;

//Define
#define WINDOW_SIZE_X 800
#define WINDOW_SIZE_Y 800
#define WINDOW_SIZE_Z 800
#define MAP_Z_SIZE 10000
#define MAX_ICE 100
#define LIGHT_X 0
#define LIGHT_Y 100
#define LIGHT_Z 150
#define deg(x) 3.141592 * x / 180

// 충돌체크 사이즈
#define PUMP_COLL 80
#define APP_COLL 60
#define TANGER_COLL 70
#define TOMA_COLL 75
#define GRAPE_COLL 60
#define ITEM_COLL 75
#define WATERM_COLL 105

// 객체 개수
#define MAX_HUDDLE 128
#define MAX_BALL 100
#define MAX_PARTICLE 100




// 전체속성
#define CREATE_HUDDLE_SPEED 45	// 15의 배수 // 장애물 나오는 속도
#define CREATE_HUDDLE_Z -8000
#define DELETE_HUDDLE_Z 0
#define DELETE_BALL_Z -8000
#define DELETE_PARTICLE_Z 1000
#define DELETE_RECT_Z 0
#define RECT_SIZE 200
#define TIME_MULTIBALL 300
#define ADD_TIME_MULTIBALL 300


#define START_STATE_INITE_NUMBER 0

#define MAX_CLIENTS 3

//particle


//Enum
enum VIEW { Perspective, Orthographic };
enum OBJECTTYPE{None, Apple, Grape, Pumkin, Tangerine, Tomato, Watermellon, Rect,Item};
enum OBJECT{Ball = 7, Particle = 8, NONE = 9};

//enum STATE { LOBBY_STATE, PLAY_STATE, END_STATE };

//Class
class Angle {
public:
	float radian;
	bool sw;

	Angle() {
		radian = 0.f;
		sw = false;
	}
};

struct COLOR {
public:
	float r;
	float g;
	float b;
	float a;

	COLOR() {}
	COLOR(float _r, float _g, float _b, float _a)
		:r(_r), g(_g), b(_b), a(_a) {}
};

//Funtion
// 대화상자 프로시저
BOOL CALLBACK DlgProc(HWND, UINT, WPARAM, LPARAM);

GLvoid drawScene(GLvoid);
GLvoid Reshape(int w, int h);
void Mouse(int button, int state, int x, int y);
void CharacterMotion(int x, int y);
void Timer(int value);
void Keyboard(unsigned char key, int x, int y);
void Change_Angle_xyz();
void Draw_bottom();
void AddParticle(float in_x, float in_y, float in_z, float in_r = 0, float in_g = 0, float in_b = 0);
int FindSlotParticleObject();
void DrawObjects();
void UpdateObjects();
void drawText(const char *text, int length, int x, int y);
void Write_FRUIT_CRUSH(float x, float y, float z);
void Write_GAME_OVER(float x, float y, float z);
void Write_GAME_CLEAR(float x, float y, float z);
void Draw_End_state_Background(float move_z);
void LoginKeboardEvent(unsigned char key);
