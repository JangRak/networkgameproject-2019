#include "TCP.h"
#include "SERVER.h"

#include "BALL.h"
#include "HUDDLE.h"
#include "APPLE.h"
#include "GRAPE.h"
#include "PUMPKIN.h"
#include "Rect.h"
#include "TANGERINE.h"
#include "TOMATO.h"
#include "WATERMELLON.h"
#include "BALL.h"
#include "NONE.h"
#include "ITEM.h"

#pragma comment(lib,"ws2_32")
#include <WinSock2.h>
#include <iostream>
#include <thread>
#include <mutex>
#include <string>
#include "Player.h"
#include "Particle.h"

#include "..\..\Server\Server\Protocol.h"

enum OBJECTTYPE { None, Apple, Grape, Pumkin, Tangerine, Tomato, Watermellon, Rect, Item };
enum STAGE { LOGIN, LOBBY, INGAME_RUN, INGAME_CLE, INGAME_OVE };
#define CREATE_HUDDLE_Z -8000

unsigned int TCP::sock;
PacketFrameState TCP::framepacket;
PacketReadyStateServerToClient TCP::lobbypacket;
PacketLoginOK TCP::loginpacket;

extern Player* player[3];
extern BALL* ball[MAX_BALL];
extern HUDDLE* huddle[MAX_HUDDLE];
extern int Score;
extern int Have_balls;
extern SERVER server;
extern std::string nickname;
extern int CurrentState;
extern PARTICLE* particle[MAX_PARTICLE];
extern int Huddle_speed;

//extern mutex statelock;

TCP::TCP() {
	wsa = new WSAData;
	serveraddr = new sockaddr_in;
	defaultCoolTime = 0;
	remainingCoolTime = defaultCoolTime;
}

TCP::~TCP() {
	delete wsa;
	delete serveraddr;
}

void TCP::err_quit(const char *msg) {
	LPVOID lpMsgBuf;
	// 오류메세지 얻기
	FormatMessage(
		// 오류메세지 저장 공간을 알아서 할당 | 운영체제로부터 오류 메세지 가져옴
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		// NULL , 오류코드 - [구체적 오류 코드 확인]
		NULL, WSAGetLastError(),
		//오류메세지 표시 언어 - 사용자 제어판 기본 설정 언어
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		// 오류 문자열 시작주소, nSize, Arguments
		(LPTSTR)&lpMsgBuf, 0, NULL);
	// 메세지 상자 생성 - [오류 메세지 확인]
	MessageBox(NULL, (LPCTSTR)lpMsgBuf, (LPCTSTR)msg, MB_ICONERROR);
	// 시스템 할당 메모리 반환
	LocalFree(lpMsgBuf);
	exit(1);
}

int TCP::recvn(unsigned int s, char *buf, int len, int flags) {
	int received;
	char *ptr = buf;
	int left = len;

	while (left > 0) {
		received = recv(s, ptr, left, flags);

		if (received == SOCKET_ERROR)
			return SOCKET_ERROR;

		else if (received == 0) {
			// std::cout << "Too much Clients connecting" << std::endl;
			closesocket(sock);
			return 0;
		}


		left -= received;
		ptr += received;
	}
	return (len - left);
}

int TCP::ConnectTCPToServer(const char* serverip) {

	if (WSAStartup(MAKEWORD(2, 2), &(*wsa)) != 0)
		return -1;

	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == INVALID_SOCKET)	err_quit("socket()");

	ZeroMemory(&(*serveraddr), sizeof((*serveraddr)));
	serveraddr->sin_family = AF_INET;
	serveraddr->sin_addr.s_addr = inet_addr(serverip);
	serveraddr->sin_port = htons(SERVERTCPPORT);

	int retval = connect(sock, (SOCKADDR *)&(*serveraddr), sizeof((*serveraddr)));
	if (retval == SOCKET_ERROR)		err_quit("connect()");

	printf("\n서버 연결: IP 주소=%s, 포트 번호=%d \n",
		inet_ntoa(serveraddr->sin_addr), ntohs(serveraddr->sin_port));



	return RecvMyID();
}

int TCP::RecvMyID() {
	int id = 0;

	int retval = recvn((SOCKET)sock, (char*)&id, sizeof(int), 0);
	if (retval <= 0) err_quit("[RECV Error] ID \n");

	bool IsLogin = true;
	//if(id)


	// std::cout << "TCP connect Success" << std::endl;
	// std::cout << "MY ID : " << id << std::endl;

	return id;
}

void TCP::ClickMouse(char id, float x, float y) {
	PacketClickMouse packet;
	packet.size = sizeof(PacketClickMouse);

	// framepacket.addball->itemType == true;
	packet.type = CLICK_MOUSE;
	packet.id = id;
	packet.x = x;
	packet.y = y;

	remainingCoolTime = server.GetFrame();
	if (remainingCoolTime - defaultCoolTime > 15) {
		defaultCoolTime = remainingCoolTime;
		int retval = send(sock, (char*)&packet, sizeof(PacketClickMouse), 0);
		if (retval == SOCKET_ERROR)		err_quit("[TCP Send Error]");
	}
}

void TCP::CreateRecvThread() {
	HANDLE RecvTCPThread = CreateThread(NULL, 0, this->RecvDataThread, NULL, 0, NULL);
	if (NULL == RecvTCPThread)
		std::cout << "Create RecvTCPThread Error" << std::endl;
}

unsigned long __stdcall TCP::RecvDataThread(void *) {

	// std::cout << "Do - Login Recv" << std::endl;

	while (true) {//;state == 0) {
		ZeroMemory(&loginpacket, sizeof(PacketLoginOK));

		int retval = recvn((SOCKET)sock, (char*)&loginpacket, sizeof(PacketLoginOK), 0);
		if (retval < 0)	err_quit("[RECV Error] ID \n");
		UpdateLogin();
		if (0 == server.GetState()) break;
	}

	// std::cout << "Do - Lobby Recv" << std::endl;

	while (true) {//;state == 0) {
		ZeroMemory(&lobbypacket, sizeof(PacketReadyStateServerToClient));
		if (server.GetState() != 0) {
			//std::cout << "대기?" << std::endl;
			// std::cout << "서버님의 state란다 애덜아 하하하핳" << server.GetState() << std::endl;
			continue;
		}
		int retval = recvn((SOCKET)sock, (char*)&lobbypacket, sizeof(PacketReadyStateServerToClient), 0);
		if (retval < 0)	err_quit("[RECV Error] ID \n");
		UpdateLobby();
		if (1 == server.GetState()) break;
	}

	// std::cout << "Do - InGame Recv" << std::endl;

	while (true) {
		//ZeroMemory(&framepacket, sizeof(PacketFrameState));

		int retval = recvn((SOCKET)sock, (char*)&framepacket, sizeof(PacketFrameState), 0);
		if (retval < 0)	err_quit("[RECV Error] ID \n");
		UpdateServerFrame();
		if (2 == server.GetState())
		{
			// std::cout << "야이이이이이이이ㅣ이이이이이잉 너는 뭔데에에ㅔ" << std::endl;
			break;
		}
	}

	//while (state == 2) {
	//	/*ZeroMemory(&framepacket, sizeof(PacketFrameState));

	//	int retval = recvn((SOCKET)sock, (char*)&framepacket, sizeof(PacketFrameState), 0);
	//	if (retval < 0)	err_quit("[RECV Error] ID \n");
	//	UpdateServerFrame();*/
	//}

	return 0;
}


void TCP::UpdateServerFrame() {
	// 패킷데이터 해석하자

	if (framepacket.type == INGAME_CLEAR) {
		// std::cout << "야이이이이이이이ㅣ이이이이이잉" << std::endl;
		server.SetState(2);
		return;
	}


	//else if (framepacket.type == INGAME_RUNNING) {
	server.AddFrame();

	//player[0]->SetHaveballs(framepacket.haveballs0);
	//player[1]->SetHaveballs(framepacket.haveballs1);
	//player[2]->SetHaveballs(framepacket.haveballs2);

	player[0]->SetPosition(framepacket.xPos[0], framepacket.yPos[0]);
	player[1]->SetPosition(framepacket.xPos[1], framepacket.yPos[1]);
	player[2]->SetPosition(framepacket.xPos[2], framepacket.yPos[2]);

	for (int i = 0; i < 15; ++i) {
		if (framepacket.collision[i].ObjIndex >= 0) {
			// std::cout << "Delete collision huddle" << std::endl;

			float x, y, z;
			if (framepacket.collision[i].ObjIndex >= 0) {
				huddle[framepacket.collision[i].ObjIndex]->GetPos(&x, &y, &z);

				AddParticle(x, y, z, 33 / 255.f, 116 / 255.f, 28 / 255.f);

				// std::cout << "INDEX :" << framepacket.collision[i].ObjIndex
				// 	<< " Y : " << huddle[framepacket.collision[i].ObjIndex]->y
				// 	<< " Y0 : " << huddle[framepacket.collision[i].ObjIndex]->y0
				// 	<< std::endl;

				delete huddle[framepacket.collision[i].ObjIndex];
				huddle[framepacket.collision[i].ObjIndex] = nullptr;
			}
		}
		if (framepacket.collision[i].Ballindex >= 0) {
			// std::cout << "Delete collision ball" << std::endl;

			delete ball[framepacket.collision[i].Ballindex];
			ball[framepacket.collision[i].Ballindex] = nullptr;
		}
	}

	for (int i = 0; i < 15; ++i) {
		int index = framepacket.deleteball[i].index;
		if (-1 < index) {
			// std::cout << "delete range out ball" << index << std::endl;
			delete ball[index];
			ball[index] = nullptr;
		}
		else {
			break;
		}
	}

	{
		int deletehuddleindex = framepacket.deletehuddle.index;
		if (-1 < deletehuddleindex) {
			// std::cout << "---delete range out huddle" << deletehuddleindex << std::endl;
			delete huddle[deletehuddleindex];
			huddle[deletehuddleindex] = nullptr;
		}
	}

	Score = framepacket.score;
	if (CurrentState != framepacket.stage) {
		CurrentState = framepacket.stage;

		switch (CurrentState) {
		case 1:
			Huddle_speed = HUDDLE_MOVE_SPEED_1;
			break;
		case 2:
			Huddle_speed = HUDDLE_MOVE_SPEED_2;
			break;
		case 3:
			Huddle_speed = HUDDLE_MOVE_SPEED_3;
			break;
		default:
			break;
		}
	}

	// 공 0개면 종료
	Have_balls = framepacket.ballCount;
	if (Have_balls < 1) {
		// std::cout << "야이이이이이이이ㅣ이이이이이잉 볼" << std::endl;
		server.SetState(2);
		return;
	}

	for (int i = 0; i < 10; ++i) {
		if (framepacket.addball[i].index < 0)   break;

		int index = (int)framepacket.addball[i].index;
		int player = (int)framepacket.addball[i].playerid;
		bool itemType = (int)framepacket.addball[i].itemType;
		float x = (float)framepacket.addball[i].x;
		float y = (float)framepacket.addball[i].y;

		ball[index] = new BALL(x, y, itemType, player, server.GetFrame());
	}


	if (framepacket.addobject.index < 0)	return;

	else {
		int index = (int)framepacket.addobject.index;
		// std::cout << "+++create huddle ID : " << index << std::endl;
		float x = (float)framepacket.addobject.x;
		float y = (float)framepacket.addobject.y;
		int type = (int)framepacket.addobject.type;
		int MoveDir = (int)framepacket.addobject.MoveDir;
		float MoveMin = (float)framepacket.addobject.MoveMin;
		float MoveMax = (float)framepacket.addobject.MoveMax;
		long long frame = framepacket.addobject.frame;
		switch (type) {
		case Apple:
			huddle[index] = new APPLE(x, y, CREATE_HUDDLE_Z, MoveDir, MoveMin, MoveMax, frame);
			break;
		case Grape:
			huddle[index] = new GRAPE(x, y, CREATE_HUDDLE_Z, MoveDir, MoveMin, MoveMax, frame);
			break;
		case Pumkin:
			huddle[index] = new PUMPKIN(x, y, CREATE_HUDDLE_Z, MoveDir, MoveMin, MoveMax, frame);
			break;
		case Tangerine:
			huddle[index] = new TANGERINE(x, y, CREATE_HUDDLE_Z, MoveDir, MoveMin, MoveMax, frame);
			break;
		case Tomato:
			huddle[index] = new TOMATO(x, y, CREATE_HUDDLE_Z, MoveDir, MoveMin, MoveMax, frame);
			break;
		case Watermellon:
			huddle[index] = new WATERMELLON(x, y, CREATE_HUDDLE_Z, MoveDir, MoveMin, MoveMax, frame);
			break;
		case Rect:
			huddle[index] = new OBSRECT(x, y, CREATE_HUDDLE_Z, MoveDir, MoveMin, MoveMax, frame);
			break;
		case Item:
			huddle[index] = new ITEM(x, y, CREATE_HUDDLE_Z, MoveDir, MoveMin, MoveMax, frame);
			break;
		case None:
			huddle[index] = new NONE(x, y, -10.f, MoveDir, MoveMin, MoveMax, frame);
			break;
		case -1: case -2: case -3:
			// std::cout << "스테이지 넘어간다요 " << std::endl;
			break;
		default:
			// std::cout << "얘가 미쳤나봐ㅏ 이상한 값을 주네 " << type << std::endl;
			break;
		}
	}
	//}
}

void TCP::UpdateLobby() {

	switch (lobbypacket.type) {
	case CLIENTS_ALL_READY:
		SendReadyClientsOK();
		server.SetState(1);
		break;

	case CLIENTS_AT_LEAST_ONE_UNREADY:
		player[0]->SetReady(lobbypacket.id_r[0]);
		player[1]->SetReady(lobbypacket.id_r[1]);
		player[2]->SetReady(lobbypacket.id_r[2]);
		break;

	case CLIENT_READY_AND_NICK:
		player[0]->SetReady(lobbypacket.id_r[0]);
		player[1]->SetReady(lobbypacket.id_r[1]);
		player[2]->SetReady(lobbypacket.id_r[2]);

		player[0]->SetNickname(lobbypacket.Nick[0]);
		player[1]->SetNickname(lobbypacket.Nick[1]);
		player[2]->SetNickname(lobbypacket.Nick[2]);

		//std::cout << "Player 01: " << lobbypacket.Nick[1] << std::endl;

		break;
	}
}

void TCP::UpdateLogin() {
	switch (loginpacket.type) {
	case NICKNAME_USE:
		// std::cout << "NICK USED YES!" << std::endl;

		server.SetState(0);
		break;

	case NICKNAME_UNUSE:
		// std::cout << "NICK USED NO!" << std::endl;
		server.ClearNick();
		break;

	default:
		break;
	}
}

// Lobby

void TCP::SendReadyClientToServer(int id) {
	PacketReadyStateClientToServer packet;
	packet.size = sizeof(PacketReadyStateClientToServer);
	packet.type = CLIENT_READY;
	packet.id = id;

	int retval = send(sock, (char*)&packet, sizeof(PacketReadyStateClientToServer), 0);
	if (retval == SOCKET_ERROR)		err_quit("[TCP Send Error]");
}

void TCP::SendUnReadyClientToServer(int id) {
	PacketReadyStateClientToServer packet;
	packet.size = sizeof(PacketReadyStateClientToServer);
	packet.type = CLIENT_UNREADY;
	packet.id = id;

	int retval = send(sock, (char*)&packet, sizeof(PacketReadyStateClientToServer), 0);
	if (retval == SOCKET_ERROR)		err_quit("[TCP Send Error]");
}

void TCP::SendReadyClientsOK() {
	PacketReadyStateClientToServer packet;
	packet.size = sizeof(PacketReadyStateClientToServer);
	packet.type = CLIENTS_READY_OK;
	packet.id = 3;

	int retval = send(sock, (char*)&packet, sizeof(PacketClickMouse), 0);
	if (retval == SOCKET_ERROR)		err_quit("[TCP Send Error]");
}

void TCP::SendTCPCanUseNicknameClientToServer(const char* nickname)
{
	PacketCanUseNick packet;
	packet.size = sizeof(PacketCanUseNick);
	packet.type = NICKNAME_CAN;
	strcpy(packet.Nick, nickname);

	int retval = send(sock, (char*)&packet, sizeof(PacketCanUseNick), 0);
	if (retval == SOCKET_ERROR)		err_quit("[TCP Login Send Vaild Nick Send Error]");

}



PacketLoginOK TCP::RecvNickOK()
{
	PacketLoginOK packet;
	ZeroMemory(&packet, sizeof(packet));
	int retval = recv(sock, (char*)&packet, sizeof(PacketLoginOK), 0);
	if (retval == SOCKET_ERROR)		err_quit("[TCP Login RecvNickOK Error]");

	return packet;
}

PacketALLNick TCP::RecvTCPClientsNicknameClientToServer()
{
	PacketALLNick packet;
	int retval = recv(sock, (char*)&packet, sizeof(PacketALLNick), 0);
	if (retval == SOCKET_ERROR)		err_quit("[TCP Login Recv Can Use Error]");
	// std::cout << retval << std::endl;
	return packet;
}

// 새로운 장애물 생성
void TCP::AddParticle(float in_x, float in_y, float in_z, float in_r, float in_g, float in_b) {
	int index = FindSlotParticleObject();
	if (index < 0)		return;

	particle[index] = new PARTICLE(in_x, in_y, in_z, in_r, in_g, in_b);
}

// particle 배열에서 빈공간 찾기
int TCP::FindSlotParticleObject() {
	for (int i = 0; i < MAX_PARTICLE; i++)
		if (particle[i] == NULL)return i;
	return -1;
}