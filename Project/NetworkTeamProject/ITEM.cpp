#include "GRAPE.h"
#include "main.h"

void ITEM::Draw(){

	GLfloat Object_specref[] = { 1.f, 1.f, 1.f, 1.f };
	GLfloat Object[] = { 0.5f, 0.5f, 0.01f, 1.f };
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref);
	glMateriali(GL_FRONT, GL_SHININESS, 64);

	glPushMatrix();
	glTranslatef(x, y, z);
	glRotatef(rad, 1.f, 0.f, 0.f);
	glRotatef(rad, 0.f, 1.f, 0.f);
	glRotatef(rad, 0.f, 0.f, 1.f);
	glutWireSphere(size, 10, 10);
	
	glPushMatrix();
	for (int i = 0; i <= 360; i += 45) {
		for (int j = 0; j <= 360; j += 45) {
			for (int k = 0; k <= 360; k += 45) {
				glPushMatrix();
				glRotatef(i, 1.f, 0.f, 0.f);
				glRotatef(j, 0.f, 1.f, 0.f);
				glRotatef(k, 0.f, 0.f, 1.f);
				glutSolidCone(50, 100, 5, 5);
				glPopMatrix();
			}
		}
	}
	glPopMatrix();

	glPopMatrix();
}

ITEM::ITEM(){

}

ITEM::ITEM(float in_x, float in_y, float in_z, int in_moving, float in_data_min, float in_data_max, long long in_frame)
	:HUDDLE(in_x, in_y, in_z, in_moving, in_data_min, in_data_max, in_frame) {
	type = Item;
	data = 0;
	size = ITEM_COLL;
	score = 100;
}


ITEM::~ITEM(){

}

