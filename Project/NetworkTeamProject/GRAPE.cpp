#include "GRAPE.h"
#include "main.h"
void GRAPE::Draw() {

	GLfloat Object_specref[] = { 1.f, 1.f, 1.f, 1.f };

	glPushMatrix();

	glTranslatef(x, y, z);
	glRotatef(rad, 0.f, 1.f, 0.f);
	glutWireSphere(size, 10, 10);

	glPushMatrix();
	glTranslatef(0.f, 30.f, 0.f);
	GLfloat Object0[] = { 0.5f, 0.1f, 0.8f, 1.f };
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object0);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref);
	glMateriali(GL_FRONT, GL_SHININESS, 64);

	glutSolidSphere(30, 100, 100); // ���
	glTranslatef(-30, 0, 0); // ������
	glutSolidSphere(30, 100, 100);

	glTranslatef(60, 0, 0);
	glutSolidSphere(30, 100, 100);

	glTranslatef(-20, -45, 0);
	glutSolidSphere(30, 100, 100);

	glTranslatef(-30, 0, 0);
	glutSolidSphere(30, 100, 100);

	glTranslatef(15, -45, 0);
	glutSolidSphere(30, 100, 100);



	glPushMatrix();
	glTranslatef(0, 160, 0);
	GLfloat Object1[] = { 0.01f, 1.f, 0.01f, 1.f };
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object1);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref);
	glMateriali(GL_FRONT, GL_SHININESS, 64);

	glPushMatrix();
	glRotatef(90.f, 1.f, 0.f, 0.f);
	glutSolidCone(10, 80, 10, 10);
	glPopMatrix();

	glPopMatrix();
	glPopMatrix();
	glPopMatrix();
}

GRAPE::GRAPE()
	:HUDDLE() {
}

GRAPE::GRAPE(float in_x, float in_y, float in_z, int in_moving, float in_data_min, float in_data_max, long long in_frame)
	: HUDDLE(in_x, in_y, in_z, in_moving, in_data_min, in_data_max, in_frame) {
	type = Grape; 
	data = 0;
	size = GRAPE_COLL;
	score = 100;
}



GRAPE::~GRAPE()
{
}