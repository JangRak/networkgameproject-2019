#pragma once

#define CLIENT_READY 5
#define CLIENT_UNREADY 6


struct PacketReadyClientToServer
{
	char size;
	char type;
	char id;
};