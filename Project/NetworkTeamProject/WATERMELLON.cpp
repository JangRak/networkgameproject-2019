#include "WATERMELLON.h"
#include "main.h"
void WATERMELLON::Draw() {
	GLfloat Object_specref[] = { 1.f, 1.f, 1.f, 1.f };

	glPushMatrix();

	glTranslatef(x, y, z);
	glutWireSphere(size, 10, 10);
	glRotatef(90.f, 1.f, 1.f, 0.f);
	glRotatef(rad, 0.f, 1.f, 0.f);

	glPushMatrix();
	GLfloat Object0[] = { 0.01f, 1.0f, 0.01f, 1.f };
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object0);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref);
	glMateriali(GL_FRONT, GL_SHININESS, 64);
	glutSolidSphere(100, 100, 100);
	glPopMatrix();

	GLfloat Object1[] = { 0.f, 0.f, 0.f, 1.f };
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, Object1);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Object_specref);
	glMateriali(GL_FRONT, GL_SHININESS, 64);
	for (float i = 0.f; i < 180.f; i += 30.f) {
		glPushMatrix();
		glRotatef(i, 0.f, 1.f, 0.f);
		glScalef(0.2f, 1.f, 1.f);
		glutSolidSphere(100 + 1, 100, 100);
		glPopMatrix();
	}
	glPopMatrix();

}

WATERMELLON::WATERMELLON()
	:HUDDLE() {
}

WATERMELLON::WATERMELLON(float in_x, float in_y, float in_z, int in_moving, float in_data_min, float in_data_max, long long in_frame)
	: HUDDLE(in_x, in_y, in_z, in_moving, in_data_min, in_data_max, in_frame) {
	type = Watermellon;
	data = 0;
	size = WATERM_COLL;
	score = 30;
}



WATERMELLON::~WATERMELLON()
{
}
